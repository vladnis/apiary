"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_service_1 = require("../../core/services/auth.service");
var login_dependent_component_1 = require("../../libs/login-dependent.component");
var NavComponent = /** @class */ (function (_super) {
    __extends(NavComponent, _super);
    function NavComponent(authService) {
        var _this = _super.call(this, authService) || this;
        _this.authService = authService;
        _this.isLoggedIn = false;
        _this.isLoggedIn = _this.authService.isLoggedIn();
        return _this;
    }
    NavComponent.prototype.preStatusChange = function () { };
    NavComponent.prototype.statusChange = function (isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    };
    NavComponent = __decorate([
        core_1.Component({
            selector: 'nav-component',
            templateUrl: './nav.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService])
    ], NavComponent);
    return NavComponent;
}(login_dependent_component_1.LoginDependentComponent));
exports.NavComponent = NavComponent;
//# sourceMappingURL=nav.component.js.map