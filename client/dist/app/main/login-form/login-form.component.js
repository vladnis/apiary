"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var abstract_form_1 = require("../../core/libs/abstract-form");
var messages_service_1 = require("../../core/services/messages.service");
var router_1 = require("@angular/router");
var auth_service_1 = require("../../core/services/auth.service");
var LoginFormComponent = /** @class */ (function (_super) {
    __extends(LoginFormComponent, _super);
    function LoginFormComponent(authService, fb, messageService, router) {
        var _this = _super.call(this, fb, messageService) || this;
        _this.authService = authService;
        _this.fb = fb;
        _this.messageService = messageService;
        _this.router = router;
        _this.errorCodeMessages = {
            200: 'Successfully logged in',
            404: 'User or password are incorrect'
        };
        _this.fields = {
            username: ['', [forms_1.Validators.required, forms_1.Validators.minLength(2), forms_1.Validators.maxLength(255)]],
            password: ['', [forms_1.Validators.required, forms_1.Validators.minLength(6), forms_1.Validators.maxLength(255)]],
        };
        _this.validationMessages = {
            username: {
                'required': 'Username is required',
                'maxlength': 'Value is too large'
            },
            password: {
                'required': 'Password is required',
                'minlength': 'Password must be at least 6 characters long',
                'maxlength': 'Value is too large'
            },
        };
        return _this;
    }
    LoginFormComponent.prototype.login = function () {
        this.setGeneralSuccessMessage('Trying to log in ...');
        var formModel = this.form.value;
        var loginInfo = {
            username: formModel.username,
            password: formModel.password,
        };
        this.authService.login(loginInfo).subscribe(this.handleSuccess.bind(this), this.handleErrors.bind(this));
    };
    LoginFormComponent = __decorate([
        core_1.Component({
            selector: 'login-from',
            templateUrl: './login-form.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService, forms_1.FormBuilder, messages_service_1.MessagesService, router_1.Router])
    ], LoginFormComponent);
    return LoginFormComponent;
}(abstract_form_1.AbstractForm));
exports.LoginFormComponent = LoginFormComponent;
//# sourceMappingURL=login-form.component.js.map