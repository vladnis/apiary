"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var base_async_component_1 = require("../../core/libs/base-async-component");
var messages_service_1 = require("../../core/services/messages.service");
var user_service_1 = require("../../core/services/user.service");
var UserInfoComponent = /** @class */ (function (_super) {
    __extends(UserInfoComponent, _super);
    /**
     * Initialise component.
     *
     * @param messageService
     * @param userService
     */
    function UserInfoComponent(messageService, userService) {
        var _this = _super.call(this, messageService) || this;
        _this.messageService = messageService;
        _this.userService = userService;
        _this.errorCodeMessages = {
            404: 'Cannot find user'
        };
        return _this;
    }
    /**
     * Get the observer for current data in component.
     *
     * @return Observable<any>
     */
    UserInfoComponent.prototype.getDataObserver = function () {
        return this.userService.getCurrentUserInfo();
    };
    /**
     * Parse response in case of success.
     */
    UserInfoComponent.prototype.parseResponse = function (response) {
        if (!response.status || response.status !== 200 || !response.data) {
            return this.setErrorMessage("Invalid response");
        }
        var data = response.data;
        if (!data.userId) {
            return this.setErrorMessage("Invalid user id");
        }
        if (!data.userName) {
            return this.setErrorMessage("Invalid user name");
        }
        this.userId = data.userId;
        this.username = data.userName;
    };
    UserInfoComponent = __decorate([
        core_1.Component({
            selector: 'user-info',
            templateUrl: './user-info.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [messages_service_1.MessagesService, user_service_1.UserService])
    ], UserInfoComponent);
    return UserInfoComponent;
}(base_async_component_1.BaseAsyncComponent));
exports.UserInfoComponent = UserInfoComponent;
//# sourceMappingURL=user-info.component.js.map