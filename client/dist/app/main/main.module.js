"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var main_routing_module_1 = require("./main-routing.module");
var core_module_1 = require("../core/core.module");
var login_form_component_1 = require("./login-form/login-form.component");
var header_component_1 = require("./header/header.component");
var footer_component_1 = require("./footer/footer.component");
var main_component_1 = require("./main.component");
var home_component_1 = require("./home/home.component");
var user_info_component_1 = require("./user-info/user-info.component");
var api_projects_component_1 = require("./api-projects/api-projects.component");
var MainModule = /** @class */ (function () {
    function MainModule() {
    }
    MainModule = __decorate([
        core_1.NgModule({
            imports: [
                core_module_1.CoreModule,
                main_routing_module_1.MainRoutingModule,
            ],
            declarations: [
                login_form_component_1.LoginFormComponent,
                header_component_1.HeaderComponent,
                footer_component_1.FooterComponent,
                main_component_1.MainComponent,
                home_component_1.HomeComponent,
                user_info_component_1.UserInfoComponent,
                api_projects_component_1.ApiProjectsComponent
            ],
            exports: [],
            providers: []
        })
    ], MainModule);
    return MainModule;
}());
exports.MainModule = MainModule;
//# sourceMappingURL=main.module.js.map