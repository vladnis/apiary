"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function matchFormControlValueValidator(matchValueKey) {
    var subscribed = false;
    return function (control) {
        if (!control.parent) {
            return {
                notMatchesOther: true
            };
        }
        var form = control.parent;
        var value = control.value;
        var otherControl = form.get(matchValueKey);
        var matchValue = otherControl.value;
        if (!subscribed) {
            subscribed = true;
            otherControl.valueChanges.subscribe(function () {
                control.updateValueAndValidity();
            });
        }
        if (value !== matchValue) {
            return {
                notMatchesOther: true
            };
        }
        return null;
    };
}
exports.matchFormControlValueValidator = matchFormControlValueValidator;
//# sourceMappingURL=matchFormControlValueValidator.js.map