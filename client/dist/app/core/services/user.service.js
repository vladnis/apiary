"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/Rx");
var core_http_service_1 = require("./core-http.service");
var messages_service_1 = require("./messages.service");
var auth_service_1 = require("./auth.service");
var http_1 = require("@angular/common/http");
var UserService = /** @class */ (function (_super) {
    __extends(UserService, _super);
    function UserService(http, messageService, authService) {
        var _this = _super.call(this, http, messageService) || this;
        _this.http = http;
        _this.messageService = messageService;
        _this.authService = authService;
        return _this;
    }
    /**
     * Get current user information.
     *
     * @returns {Observable<R>}
     */
    UserService.prototype.getCurrentUserInfo = function () {
        return this.get('user', {});
    };
    /**
     * Get current user information.
     *
     * @returns {Observable<R>}
     */
    UserService.prototype.getCurrentUserApiProjects = function () {
        return this.get('user/api-projects', {});
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, messages_service_1.MessagesService, auth_service_1.AuthService])
    ], UserService);
    return UserService;
}(core_http_service_1.CoreHttpService));
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map