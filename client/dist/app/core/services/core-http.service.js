"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
require("rxjs/Rx");
var messages_service_1 = require("./messages.service");
var http_1 = require("@angular/common/http");
var http_2 = require("@angular/common/http");
var http_3 = require("@angular/common/http");
var config_1 = require("../../config/config");
var CoreHttpService = /** @class */ (function () {
    function CoreHttpService(http, messageService) {
        this.http = http;
        this.messageService = messageService;
        this.host = config_1.Config.endpointHost;
    }
    CoreHttpService.prototype.extractData = function (res) {
        var body = res;
        if (body && body.hasOwnProperty("data") && body.hasOwnProperty("count")) {
            return {
                data: body.data,
                count: body.count,
                status: 200
            };
        }
        return {
            data: body,
            status: 200
        };
    };
    /**
     * Read error from a body response;
     * @param error
     * @returns {any}
     */
    CoreHttpService.prototype.getErrorFromTheBody = function (error) {
        var response = false;
        try {
            response = error.json();
        }
        catch (e) {
            return false;
        }
        if (response.hasOwnProperty("errors")) {
            return response.errors;
        }
        if (response.hasOwnProperty("errorMessage")) {
            return response.errorMessage;
        }
        return false;
    };
    /**
     * Handle error from request
     *
     * @param error
     * @returns {any}
     */
    CoreHttpService.prototype.handleError = function (error) {
        if (error instanceof http_3.HttpErrorResponse) {
            // Try to read error from body.
            var errorMsg = this.getErrorFromTheBody(error);
            if (!errorMsg) {
                return Observable_1.Observable.throw({ status: error.status });
            }
            return Observable_1.Observable.throw({ status: error.status, message: errorMsg });
        }
        return Observable_1.Observable.throw({ status: 0 });
    };
    /**
     * Send get request
     *
     * @param method
     * @param params
     * @returns {Observable<>}
     */
    CoreHttpService.prototype.get = function (method, params) {
        var httpParams = new http_1.HttpParams();
        for (var key in params) {
            if (!params.hasOwnProperty(key)) {
                continue;
            }
            if (typeof params[key] === 'object' && params[key] !== null) {
                params[key] = JSON.stringify(params[key]);
            }
            httpParams = httpParams.set(key, params[key]);
        }
        return this.http.get(this.host + method, { withCredentials: true, params: httpParams })
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    };
    /**
     * Send post request
     *
     * @param method
     * @param params
     * @returns {Observable<any>}
     */
    CoreHttpService.prototype.post = function (method, params) {
        var headers = new http_2.HttpHeaders({ 'Content-Type': 'application/json' });
        return this.http.post(this.host + method, params, { headers: headers, withCredentials: true })
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    };
    /**
     * Send post request
     *
     * @param method
     * @param params
     * @returns {Observable<>}
     */
    CoreHttpService.prototype.put = function (method, params) {
        var options = {};
        var headers = new http_2.HttpHeaders({ 'Content-Type': 'application/json' });
        options.headers = headers;
        options.withCredentials = true;
        return this.http.put(this.host + method, params, options)
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    };
    /**
     * Send post request
     *
     * @param method
     * @param params
     * @returns {Observable<>}
     */
    CoreHttpService.prototype.delete = function (method, params) {
        var headers = new http_2.HttpHeaders({ 'Content-Type': 'application/json' });
        params.headers = headers;
        params.withCredentials = true;
        return this.http.delete(this.host + method, params)
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    };
    /**
     * Get current host.
     *
     * @returns {string}
     */
    CoreHttpService.prototype.getHost = function () {
        return this.host;
    };
    CoreHttpService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, messages_service_1.MessagesService])
    ], CoreHttpService);
    return CoreHttpService;
}());
exports.CoreHttpService = CoreHttpService;
//# sourceMappingURL=core-http.service.js.map