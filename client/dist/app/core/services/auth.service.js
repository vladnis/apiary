"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
var core_http_service_1 = require("./core-http.service");
var messages_service_1 = require("./messages.service");
var Subject_1 = require("rxjs/Subject");
var http_1 = require("@angular/common/http");
var AuthService = /** @class */ (function (_super) {
    __extends(AuthService, _super);
    /**
     * Initialise service.
     * @param http
     * @param messageService
     */
    function AuthService(http, messageService) {
        var _this = _super.call(this, http, messageService) || this;
        _this.http = http;
        _this.messageService = messageService;
        _this.isUserLoggedIn = undefined;
        _this.createCurrentUserObservable();
        _this.requestCurrentUser();
        return _this;
    }
    AuthService_1 = AuthService;
    /**
     * Log in.
     * @param loginInfo
     * @returns {Observable<any>}
     */
    AuthService.prototype.login = function (loginInfo) {
        var _this = this;
        var loginObservable = this.post('auth/login', loginInfo);
        return loginObservable
            .map(function (result) {
            if (!result || !result.status || result.status !== 200) {
                return;
            }
            _this.isUserLoggedIn = true;
            _this.authenticationSubject.next({ event: AuthService_1.EVENT_LOG_IN });
        }).catch(function (error) {
            _this.authenticationSubject.next({ event: AuthService_1.EVENT_LOG_OUT });
            return Observable_1.Observable.throw(error);
        });
    };
    /**
     * Log out.
     * @returns {Observable<R>}
     */
    AuthService.prototype.logout = function () {
        var _this = this;
        var logoutObservable = this.post('auth/logout', {});
        return logoutObservable.map(function (result) {
            if (!result || !result.status || result.status !== 200) {
                return;
            }
            _this.isUserLoggedIn = false;
            _this.authenticationSubject.next({ event: AuthService_1.EVENT_LOG_OUT });
        }).catch(function (error) {
            return Observable_1.Observable.throw(error);
        });
    };
    /**
     * Get the user that is logged in.
     * @returns {Observable<User>}
     */
    AuthService.prototype.getAuthObservable = function () {
        return this.authenticationSubject;
    };
    /**
     * Get current login status.
     * @returns {boolean}
     */
    AuthService.prototype.isLoggedIn = function () {
        return this.isUserLoggedIn;
    };
    /**
     * Create subject used for sending events from this service.
     */
    AuthService.prototype.createCurrentUserObservable = function () {
        this.authenticationSubject = new Subject_1.Subject();
    };
    /**
     * Request user associated with current session.
     */
    AuthService.prototype.requestCurrentUser = function () {
        var _this = this;
        this.get('auth', {})
            .subscribe(function (result) {
            _this.isUserLoggedIn = true;
            _this.authenticationSubject.next({ event: AuthService_1.EVENT_LOG_IN });
        }, function (error) {
            _this.isUserLoggedIn = false;
            _this.authenticationSubject.next({ event: AuthService_1.EVENT_LOG_OUT });
        });
    };
    AuthService.EVENT_LOG_IN = 'event_logged_in';
    AuthService.EVENT_LOG_OUT = 'event_logged_out';
    AuthService = AuthService_1 = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            messages_service_1.MessagesService])
    ], AuthService);
    return AuthService;
    var AuthService_1;
}(core_http_service_1.CoreHttpService));
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map