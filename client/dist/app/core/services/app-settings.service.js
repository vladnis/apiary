"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_http_service_1 = require("./core-http.service");
var Subject_1 = require("rxjs/Subject");
var messages_service_1 = require("./messages.service");
var http_1 = require("@angular/common/http");
var AppSettingsService = /** @class */ (function (_super) {
    __extends(AppSettingsService, _super);
    // private socket = io('http://localhost:4000');
    /**
     * Initialise service.
     * @param http
     * @param messageService
     */
    function AppSettingsService(http, messageService) {
        var _this = _super.call(this, http, messageService) || this;
        _this.http = http;
        _this.messageService = messageService;
        _this.subject = new Subject_1.Subject();
        _this.requestSettings();
        return _this;
    }
    /**
     * Log in.
     * @returns {Observable<R>}
     */
    AppSettingsService.prototype.getSettingsObservable = function () {
        return this.subject;
    };
    /**
     * Get currently logged user.
     * @returns {User}
     */
    AppSettingsService.prototype.getSettings = function () {
        return this.settings;
    };
    /**
     * Get all available languages.
     *
     * @returns {Observable<R>}
     */
    AppSettingsService.prototype.getLanguages = function () {
        return this.get('languages', {})
            .map(function (result) {
            if (!result.data) {
                return;
            }
            return result.data;
        }, function (error) { });
    };
    /**
     * Set application settings.
     * @returns {Observable<R>}
     */
    AppSettingsService.prototype.setSettings = function (languages) {
        var _this = this;
        return this.post('languages', languages)
            .map(function (result) {
            if (!result.data) {
                return;
            }
            _this.settings = result.data;
            _this.subject.next(_this.settings);
        }, function (error) { });
    };
    /**
     * Request user associated with current session.
     */
    AppSettingsService.prototype.requestSettings = function () {
        var _this = this;
        this.get('settings', {})
            .subscribe(function (result) {
            if (!result.data) {
                return;
            }
            _this.settings = result.data;
            _this.subject.next(_this.settings);
        }, function (error) { });
    };
    AppSettingsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, messages_service_1.MessagesService])
    ], AppSettingsService);
    return AppSettingsService;
}(core_http_service_1.CoreHttpService));
exports.AppSettingsService = AppSettingsService;
//# sourceMappingURL=app-settings.service.js.map