"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/Rx");
var MessagesService = /** @class */ (function () {
    /**
     * Initialise service.
     */
    function MessagesService() {
        this.defaultMessageMappings = {
            404: 'Service not available',
            0: 'Service not available'
        };
        /**
         * Default message-http code mappings.
         * @type {{}}
         */
        this.messageMappings = {};
        /**
         * Message used in case no mapping is found.
         * @type {string}
         */
        this.undefinedMessage = 'Unexpected error occurred';
        for (var i in this.defaultMessageMappings) {
            if (!this.defaultMessageMappings.hasOwnProperty(i)) {
                continue;
            }
            this.messageMappings[i] = this.defaultMessageMappings[i];
        }
    }
    /**
     * Register new user.
     *
     * @param code
     * @param context
     * @returns {string|Observable<R>}
     */
    MessagesService.prototype.getMessageFromHttpCode = function (code, context) {
        if (this.messageMappings.hasOwnProperty(context)) {
            if (this.messageMappings[context].hasOwnProperty(code)) {
                return this.messageMappings[context][code];
            }
            else {
                return this.undefinedMessage;
            }
        }
        if (this.messageMappings.hasOwnProperty(code)) {
            return this.messageMappings[code];
        }
        return this.undefinedMessage;
    };
    /**
     * Add new code to message mappings.
     * @param messages
     * @param context
     */
    MessagesService.prototype.addMessages = function (messages, context) {
        if (!this.messageMappings[context]) {
            this.messageMappings[context] = {};
        }
        for (var code in messages) {
            if (!messages.hasOwnProperty(code)) {
                continue;
            }
            this.messageMappings[context][code] = messages[code];
        }
    };
    MessagesService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], MessagesService);
    return MessagesService;
}());
exports.MessagesService = MessagesService;
//# sourceMappingURL=messages.service.js.map