"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/Rx");
var core_http_service_1 = require("./core-http.service");
var coreCRUDService = /** @class */ (function (_super) {
    __extends(coreCRUDService, _super);
    /**
     * Create new service.
     * @param http
     * @param messageService
     */
    function coreCRUDService(http, messageService) {
        var _this = _super.call(this, http, messageService) || this;
        _this.http = http;
        _this.messageService = messageService;
        return _this;
    }
    /**
     * Get all proxy user lists.
     * @param state
     * @returns {Observable<R>}
     */
    coreCRUDService.prototype.getItems = function (state) {
        return this.get(this.resource, state);
    };
    /**
     * Get one item by id.
     * @param itemId
     */
    coreCRUDService.prototype.getItem = function (itemId) {
        return this.get(this.resource + "/" + itemId, {});
    };
    /**
     * Delete proxy user list.
     * @param id
     * @returns {Observable<R>}
     */
    coreCRUDService.prototype.deleteItem = function (id) {
        return this.delete(this.resource + '/' + id, {});
    };
    /**
     * Add new proxy users lists.
     * @param item
     * @returns {Observable<R>}
     */
    coreCRUDService.prototype.createItem = function (item) {
        return this.put(this.resource, item);
    };
    /**
     * Add new proxy users lists.
     * @param item
     * @returns {Observable<R>}
     */
    coreCRUDService.prototype.updateItem = function (item) {
        if (!item._id) {
            return;
        }
        return this.post(this.resource + '/' + item._id, item);
    };
    return coreCRUDService;
}(core_http_service_1.CoreHttpService));
exports.coreCRUDService = coreCRUDService;
//# sourceMappingURL=coreCRUD.service.js.map