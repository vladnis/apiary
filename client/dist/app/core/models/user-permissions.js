"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 03.06.2017.
 */
var UserPermissions = /** @class */ (function () {
    function UserPermissions() {
    }
    UserPermissions.PERMISSION_ADMIN = "admin";
    return UserPermissions;
}());
exports.UserPermissions = UserPermissions;
//# sourceMappingURL=user-permissions.js.map