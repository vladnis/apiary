"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 03.06.2017.
 */
var User = /** @class */ (function () {
    function User(params) {
        if (params.email) {
            this.email = params.email;
        }
        if (params.surname) {
            this.surname = params.surname;
        }
        if (params.firstName) {
            this.firstName = params.firstName;
        }
        if (params.username) {
            this.username = params.username;
        }
        if (params.permissions) {
            this.permissions = params.permissions;
        }
        if (params.balance) {
            this.balance = params.balance;
        }
        if (params.birthdate) {
            this.birthdate = params.birthdate;
        }
        if (params._id) {
            this._id = params._id;
        }
        if (params.activated) {
            this.activated = params.activated;
        }
        if (params.lastLogin) {
            this.lastLogin = params.lastLogin;
        }
        if (params.createdAt) {
            this.createdAt = params.createdAt;
        }
    }
    User.prototype.hasPermission = function (permission) {
        if (!this.permissions) {
            return false;
        }
        return this.permissions.indexOf(permission) !== -1;
    };
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.js.map