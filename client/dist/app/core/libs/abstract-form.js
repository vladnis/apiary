"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractForm = /** @class */ (function () {
    /**
     * Construct new form.
     * @param fb
     * @param messageService
     */
    function AbstractForm(fb, messageService) {
        this.fb = fb;
        this.messageService = messageService;
        /**
         * Errors specific to fields.
         * @type {{}}
         */
        this.errors = {};
        /**
         * General success message.
         * @type {string}
         */
        this.successMessage = '';
        /**
         * General error message.
         * @type {string}
         */
        this.errorMessage = '';
        /**
         * Reset form on success?
         */
        this.resetFormOnSuccess = true;
        /**
         * Are the fields of the form created async?. This means that the form will not be created in the constructor.
         * The form will be created from the child.
         * @type {boolean}
         */
        this.createFormAtInit = true;
        /**
         * Displays the state of the form fields creation.
         * @type {boolean}
         */
        this.createdFormFields = false;
    }
    /**
     * Initialize form.
     */
    AbstractForm.prototype.ngOnInit = function () {
        var _this = this;
        this.messageService.addMessages(this.errorCodeMessages, this.constructor.name);
        if (this.createFormAtInit) {
            this.createForm();
            this.createdFormFields = true;
        }
        else {
            this.createdFormFields = false;
            this.createFormFields(function () {
                _this.createForm();
                _this.createdFormFields = true;
            });
        }
    };
    /**
     * Check if the form field was created.
     * @returns {boolean}
     */
    AbstractForm.prototype.isFormFieldsCreated = function () {
        return this.createdFormFields;
    };
    /**
     * Should be implemented by child when async is true.
     */
    AbstractForm.prototype.createFormFields = function (next) {
        throw new Error("Child should implement createFormFields");
    };
    /**
     * Create a new form from the specified fields.
     */
    AbstractForm.prototype.createForm = function () {
        var _this = this;
        this.form = this.fb.group(this.fields);
        this.form.valueChanges
            .subscribe(function (data) { return _this.onValueChanged(data); });
        this.onValueChanged();
        this.postCreateForm();
    };
    /**
     * Called after the form was initiated.
     */
    AbstractForm.prototype.postCreateForm = function () { };
    /**
     * Called when one value of the fields is changed.
     * @param data
     */
    AbstractForm.prototype.onValueChanged = function (data) {
        if (!this.form) {
            return;
        }
        this.resetErrors();
        var form = this.form;
        for (var field in this.fields) {
            var control = form.get(field);
            if (!control || control.pristine || control.valid) {
                continue;
            }
            var messages = this.validationMessages[field];
            for (var key in control.errors) {
                this.errors[field].push(messages[key]);
            }
        }
    };
    /**
     * Reset messages and values associated to the current form.
     */
    AbstractForm.prototype.resetForm = function () {
        this.resetErrors();
        this.form.reset();
    };
    /**
     * Reset messages associated with current form.
     */
    AbstractForm.prototype.resetErrors = function () {
        this.errorMessage = '';
        this.successMessage = '';
        for (var field in this.fields) {
            this.errors[field] = [];
        }
    };
    /**
     * Handle a successful response.
     */
    AbstractForm.prototype.handleSuccess = function () {
        if (this.resetFormOnSuccess) {
            this.resetForm();
        }
        this.setGeneralSuccessMessage(this.messageService.getMessageFromHttpCode(200, this.constructor.name));
    };
    /**
     * Handle errors.
     * @param error
     */
    AbstractForm.prototype.handleErrors = function (error) {
        this.resetErrors();
        var statusCode = false;
        var message = false;
        if (error.hasOwnProperty('status')) {
            statusCode = error.status;
        }
        if (error.hasOwnProperty('message')) {
            message = error.message;
        }
        if (typeof message == 'string') {
            this.errorMessage = message;
            return;
        }
        if (message instanceof Object) {
            for (var field in message) {
                if (!this.errors.hasOwnProperty(field) || !message.hasOwnProperty(field) || !message[field].hasOwnProperty('message')) {
                    continue;
                }
                var messageText = message[field].message;
                this.errors[field].push(messageText);
            }
            return;
        }
        if (statusCode !== false) {
            this.errorMessage = this.messageService.getMessageFromHttpCode(statusCode, this.constructor.name);
        }
    };
    /**
     * Add a new error message specific to a field.
     *
     * @param {string} field
     * @param {string} message
     */
    AbstractForm.prototype.addFieldError = function (field, message) {
        if (!this.errors.hasOwnProperty(field)) {
            throw ('Cannot add error. Invalid field: ' + field);
        }
        this.errors[field].push(message);
    };
    /**
     * Set general success message.
     * @param {string} error
     */
    AbstractForm.prototype.setGeneralErrorMessage = function (error) {
        this.resetErrors();
        this.errorMessage = error;
    };
    /**
     * Set general error message.
     * @param {string} message
     */
    AbstractForm.prototype.setGeneralSuccessMessage = function (message) {
        this.resetErrors();
        this.successMessage = message;
    };
    return AbstractForm;
}());
exports.AbstractForm = AbstractForm;
//# sourceMappingURL=abstract-form.js.map