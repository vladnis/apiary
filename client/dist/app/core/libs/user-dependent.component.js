"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var auth_service_1 = require("../services/auth.service");
var UserDependentComponent = /** @class */ (function () {
    function UserDependentComponent(authService) {
        this.authService = authService;
        this.isLoading = true;
    }
    UserDependentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isLoggedIn = this.authService.isLoggedIn();
        if (this.isLoggedIn !== undefined) {
            this.isLoading = false;
        }
        this.authService.getAuthObservable().subscribe(function (message) {
            _this.isLoading = false;
            if (!message.event) {
                return;
            }
            var event = message.event;
            if (event === auth_service_1.AuthService.EVENT_LOG_OUT) {
                _this.isLoggedIn = false;
            }
            else if (event === auth_service_1.AuthService.EVENT_LOG_IN) {
                _this.isLoggedIn = true;
            }
        });
    };
    return UserDependentComponent;
}());
exports.UserDependentComponent = UserDependentComponent;
//# sourceMappingURL=user-dependent.component.js.map