"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_form_1 = require("./abstract-form");
var AbstractCRUDModal = /** @class */ (function (_super) {
    __extends(AbstractCRUDModal, _super);
    /**
     * Initialise modal.
     *
     * @param fb
     * @param messageService
     * @param manageModalService
     */
    function AbstractCRUDModal(fb, messageService, manageModalService) {
        var _this = _super.call(this, fb, messageService) || this;
        _this.fb = fb;
        _this.messageService = messageService;
        _this.manageModalService = manageModalService;
        /**
         * Show and hide modal based on this value.
         * @type {boolean}
         */
        _this.displayModal = false;
        _this.manageModalService.getEventObservable().subscribe(function (event) {
            if (!event || !event.header || !event.item || !event.action) {
                return;
            }
            _this.resetForm();
            _this.resetErrors();
            _this.onShow();
            _this.displayModal = true;
            _this.item = event.item;
            _this.header = event.header;
            _this.action = event.action;
            for (var field in _this.fields) {
                if (!_this.item[field]) {
                    continue;
                }
                _this.form.controls[field].setValue(_this.item[field]);
            }
        });
        return _this;
    }
    AbstractCRUDModal.prototype.onShow = function () {
    };
    /**
     * Submit changes.
     */
    AbstractCRUDModal.prototype.submit = function () {
        var _this = this;
        this.setGeneralSuccessMessage('Saving changes ...');
        this.prepareRecordsForSubmit();
        var formModel = this.form.value;
        var formValues = {};
        for (var field in this.fields) {
            if (!formModel[field]) {
                continue;
            }
            formValues[field] = formModel[field];
        }
        if (this.item._id) {
            formValues['_id'] = this.item._id;
        }
        var service = null;
        switch (this.action) {
            case AbstractCRUDModal.ACTION_TYPE_CREATE:
                service = this.getCreateServiceObservable.bind(this);
                break;
            case AbstractCRUDModal.ACTION_TYPE_READ:
                service = this.getReadServiceObservable.bind(this);
                break;
            case AbstractCRUDModal.ACTION_TYPE_UPDATE:
                service = this.getUpdateServiceObservable.bind(this);
                break;
            case AbstractCRUDModal.ACTION_TYPE_DELETE:
                service = this.getDeleteServiceObservable.bind(this);
                break;
            default:
                throw "Invalid submit action";
        }
        service(formValues).subscribe(function () {
            _this.handleSuccess.bind(_this);
            _this.manageModalService.getEventObservable().next();
            _this.displayModal = false;
        }, this.handleErrors.bind(this));
    };
    AbstractCRUDModal.ACTION_TYPE_CREATE = "create";
    AbstractCRUDModal.ACTION_TYPE_UPDATE = "update";
    AbstractCRUDModal.ACTION_TYPE_DELETE = "delete";
    AbstractCRUDModal.ACTION_TYPE_READ = "delete";
    return AbstractCRUDModal;
}(abstract_form_1.AbstractForm));
exports.AbstractCRUDModal = AbstractCRUDModal;
//# sourceMappingURL=abstract-CRUD-modal.js.map