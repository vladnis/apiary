"use strict";
/**
 * Created by vlad on 01.06.2017.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var abstract_form_1 = require("./abstract-form");
var AbstractLazyForm = /** @class */ (function (_super) {
    __extends(AbstractLazyForm, _super);
    function AbstractLazyForm() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * Are the fields of the form created async?. This means that the form will not be created in the constructor.
         * The form will be created from the child.
         * @type {boolean}
         */
        _this.createFormAtInit = false;
        /**
         * Number of services loaded.
         *
         * @type {number}
         */
        _this.loadedServices = 0;
        return _this;
    }
    /**
     * Call callback when one service is loaded.
     *
     * @param next
     * @returns {any}
     */
    AbstractLazyForm.prototype.serviceLoaded = function (next) {
        this.loadedServices++;
        if (this.loadedServices >= this.totalNumberOfServices) {
            return next();
        }
    };
    return AbstractLazyForm;
}(abstract_form_1.AbstractForm));
exports.AbstractLazyForm = AbstractLazyForm;
//# sourceMappingURL=abstract-lazy-form.js.map