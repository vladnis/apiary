"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 27.08.2017.
 */
var CustomCrypto = /** @class */ (function () {
    function CustomCrypto() {
    }
    /**
     * Generate random string.
     *
     * @param len
     */
    CustomCrypto.generateId = function (len) {
        if (!len) {
            len = 20;
        }
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < len; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    };
    return CustomCrypto;
}());
exports.CustomCrypto = CustomCrypto;
//# sourceMappingURL=custom-crypto.js.map