"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 27.08.2017.
 */
var BaseAsyncComponent = /** @class */ (function () {
    /**
     * Initialise class.
     *
     * @param messageService
     */
    function BaseAsyncComponent(messageService) {
        this.messageService = messageService;
        /**
         * General error message.
         * @type {string}
         */
        this.errorMessage = '';
        this.isLoading = true;
    }
    /**
     * Initialize.
     */
    BaseAsyncComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.messageService.addMessages(this.errorCodeMessages, this.constructor.name);
        this.getDataObserver().subscribe(function (response) {
            _this.isLoading = false;
            _this.parseResponse(response);
        }, function (error) {
            _this.isLoading = false;
            _this.handleError(error);
        });
    };
    /**
     * Reset messages associated with current form.
     */
    BaseAsyncComponent.prototype.resetError = function () {
        this.errorMessage = '';
    };
    /**
     * Handle errors.
     * @param error
     */
    BaseAsyncComponent.prototype.handleError = function (error) {
        this.resetError();
        var statusCode = false;
        var message = false;
        if (error.hasOwnProperty('status')) {
            statusCode = error.status;
        }
        if (error.hasOwnProperty('message')) {
            message = error.message;
        }
        if (typeof message == 'string') {
            this.errorMessage = message;
            return;
        }
        if (statusCode !== false) {
            this.errorMessage = this.messageService.getMessageFromHttpCode(statusCode, this.constructor.name);
        }
    };
    /**
     * Send error message.
     *
     * @param message
     */
    BaseAsyncComponent.prototype.setErrorMessage = function (message) {
        this.errorMessage = message;
    };
    return BaseAsyncComponent;
}());
exports.BaseAsyncComponent = BaseAsyncComponent;
//# sourceMappingURL=base-async-component.js.map