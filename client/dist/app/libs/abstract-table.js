"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AbstractTable = /** @class */ (function () {
    function AbstractTable(messageService, manageModalService) {
        this.messageService = messageService;
        this.manageModalService = manageModalService;
        /**
         * Is the table loading?
         * @type {boolean}
         */
        this.loading = false;
        /**
         * General success message.
         * @type {string}
         */
        this.successMessage = '';
        /**
         * General error message.
         * @type {string}
         */
        this.errorMessage = '';
    }
    AbstractTable.prototype.ngOnInit = function () {
        this.messageService.addMessages(this.errorCodeMessages, this.constructor.name);
    };
    /**
     * Load table data.
     * @param event
     * @param callback
     */
    AbstractTable.prototype.loadData = function (event, callback) {
        var _this = this;
        this.lastSentEvent = event;
        this.loading = true;
        this.getItemsObservable(event).subscribe(function (res) {
            _this.loading = false;
            _this.items = res.data;
            _this.count = res.count;
            if (callback != undefined) {
                return callback();
            }
        }, this.handleMessages.bind(this));
    };
    /**
     * Delete admin
     * @param item
     * @param index
     */
    AbstractTable.prototype.deleteItem = function (item, index) {
        var _this = this;
        this.getDeleteItemObservable(item, index).subscribe(function (res) {
            if (res.status !== 200) {
                return;
            }
            _this.items = _this.items.slice(0, index).concat(_this.items.slice(index + 1, _this.items.length));
            _this.handleMessages(res, "deleteItem");
        }, function (error) {
            _this.handleMessages(error, "deleteItem");
        });
    };
    /**
     * Set general success message.
     * @param {string} error
     */
    AbstractTable.prototype.setErrorMessage = function (error) {
        this.resetErrors();
        this.errorMessage = error;
    };
    /**
     * Set general error message.
     * @param {string} message
     */
    AbstractTable.prototype.setSuccessMessage = function (message) {
        this.resetErrors();
        this.successMessage = message;
    };
    /**
     * Reset messages associated with current form.
     */
    AbstractTable.prototype.resetErrors = function () {
        this.errorMessage = '';
        this.successMessage = '';
    };
    /**
     * Reload table data.
     * @param {any} callback
     */
    AbstractTable.prototype.reloadTable = function (callback) {
        if (!this.lastSentEvent) {
            return;
        }
        this.loadData(this.lastSentEvent, callback);
    };
    /**
     * Handle errors.
     *
     * @param error
     * @param args
     */
    AbstractTable.prototype.handleMessages = function (error) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var context = null;
        if (args.hasOwnProperty(0)) {
            context = args[0];
        }
        this.resetErrors();
        var statusCode = false;
        if (error.hasOwnProperty('status')) {
            statusCode = error.status;
        }
        return this.errorMessage = this.messageService.getMessageFromHttpCode(statusCode, context || this.constructor.name);
    };
    /**
     * Send information to modal.
     *
     * @param action
     * @param header
     * @param item
     */
    AbstractTable.prototype.manageItemModal = function (action, header, item) {
        this.manageModalService.getEventObservable().next({
            action: action,
            header: header,
            item: item
        });
    };
    return AbstractTable;
}());
exports.AbstractTable = AbstractTable;
//# sourceMappingURL=abstract-table.js.map