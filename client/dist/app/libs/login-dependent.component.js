"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 06.06.2017.
 */
var LoginDependentComponent = /** @class */ (function () {
    function LoginDependentComponent(authService) {
        this.authService = authService;
    }
    LoginDependentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getAuthObservable().subscribe(function (message) {
            _this.preStatusChange();
            if (!message.user) {
                return _this.statusChange(false);
            }
            _this.statusChange(true);
        });
    };
    return LoginDependentComponent;
}());
exports.LoginDependentComponent = LoginDependentComponent;
//# sourceMappingURL=login-dependent.component.js.map