"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 06.06.2017.
 */
var AppSettingsDependentComponent = /** @class */ (function () {
    function AppSettingsDependentComponent(settingService) {
        this.settingService = settingService;
    }
    AppSettingsDependentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.settingService.getSettingsObservable().subscribe(function (message) {
            if (!message) {
                return;
            }
            _this.settingsChange(message);
        });
    };
    return AppSettingsDependentComponent;
}());
exports.AppSettingsDependentComponent = AppSettingsDependentComponent;
//# sourceMappingURL=app-settings-dependent.component.js.map