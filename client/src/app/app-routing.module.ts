import { NgModule }             from '@angular/core';
import { RouterModule } from '@angular/router';
import {PageNotFoundComponent} from "./core/page-not-found/not-found.component";

@NgModule({
  imports: [
    RouterModule.forRoot([
        { path: '',   redirectTo: '/main', pathMatch: 'full' },
        { path: '**', component: PageNotFoundComponent },
    ])
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AppRoutingModule { }
