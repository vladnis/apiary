import { CommonModule }      from '@angular/common';
import {NgModule, Component, ElementRef, NgZone, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {PageNotFoundComponent} from "./page-not-found/not-found.component";
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import {RouterModule} from "@angular/router";
import {MessagesService} from "./services/messages.service";

import {UserService} from "./services/user.service";
import {AuthService} from "./services/auth.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
    imports:      [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
    ],
    declarations: [
        PageNotFoundComponent,
    ],
    exports:      [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
    ],
    providers:    [
        MessagesService,
        AuthService,
        UserService,
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]

})
export class CoreModule {}
