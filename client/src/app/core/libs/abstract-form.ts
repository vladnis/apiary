import {FormBuilder, FormGroup} from "@angular/forms";
import {OnInit} from "@angular/core";
import {MessagesService} from "../services/messages.service";

export abstract class AbstractForm implements OnInit  {

    /**
     * This needs to be implemented by child. The fields of the form.
     * @type {{}}
     */
    abstract fields:any;

    /**
     * This needs to be implemented by child.
     * @type {{}}
     */
    abstract validationMessages:any;

    /**
     * This needs to be implemented by child. Shows the messages associated with different http codes.
     * @type {{}}
     */
    abstract errorCodeMessages: any;

    /**
     * Errors specific to fields.
     * @type {{}}
     */
    errors: any = {};

    /**
     * General success message.
     * @type {string}
     */
    successMessage: string = '';

    /**
     * General error message.
     * @type {string}
     */
    errorMessage: string = '';

    /**
     * The form.
     */
    form: FormGroup;

    /**
     * Reset form on success?
     */
    resetFormOnSuccess: boolean = true;

    /**
     * Are the fields of the form created async?. This means that the form will not be created in the constructor.
     * The form will be created from the child.
     * @type {boolean}
     */
    createFormAtInit = true;

    /**
     * Displays the state of the form fields creation.
     * @type {boolean}
     */
    createdFormFields = false;


    /**
     * Construct new form.
     * @param fb
     * @param messageService
     */
    constructor(protected fb: FormBuilder, protected messageService: MessagesService) {}


    /**
     * Initialize form.
     */
    ngOnInit(): void {

        this.messageService.addMessages(this.errorCodeMessages, this.constructor.name);

        if (this.createFormAtInit) {
            this.createForm();
            this.createdFormFields = true;
        } else {
            this.createdFormFields = false;
            this.createFormFields(() => {
                this.createForm();
                this.createdFormFields = true;
            });
        }
    }

    /**
     * Check if the form field was created.
     * @returns {boolean}
     */
    public isFormFieldsCreated() {
        return this.createdFormFields;
    }

    /**
     * Should be implemented by child when async is true.
     */
    protected createFormFields(next: any) {
        throw new Error("Child should implement createFormFields");
    }

    /**
     * Create a new form from the specified fields.
     */
    protected createForm() {
        this.form = this.fb.group(this.fields);
        this.form.valueChanges
            .subscribe(data => this.onValueChanged(data));
        this.onValueChanged();
        this.postCreateForm();
    }

    /**
     * Called after the form was initiated.
     */
    protected postCreateForm() {}

    /**
     * Called when one value of the fields is changed.
     * @param data
     */
    protected onValueChanged(data?: any) {
        if (!this.form) {
            return;
        }

        this.resetErrors();

        const form = this.form;

        for (const field in this.fields) {
            const control = form.get(field);
            if (!control || control.pristine || control.valid) {
                continue;
            }
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
                this.errors[field].push(messages[key]);
            }
        }

    }

    /**
     * Reset messages and values associated to the current form.
     */
    resetForm() {
        this.resetErrors();
        this.form.reset();
    }

    /**
     * Reset messages associated with current form.
     */
    protected resetErrors() {
        this.errorMessage = '';
        this.successMessage = '';

        for (const field in this.fields) {
            this.errors[field] = [];
        }
    }

    /**
     * Handle a successful response.
     */
    protected handleSuccess() {
        if (this.resetFormOnSuccess) {
            this.resetForm();
        }
        this.setGeneralSuccessMessage(this.messageService.getMessageFromHttpCode(200, this.constructor.name));
    }

    /**
     * Handle errors.
     * @param error
     */
    protected handleErrors(error: any) {

        this.resetErrors();

        let statusCode:number|boolean = false;
        let message:boolean|any = false;

        if (error.hasOwnProperty('status')) {
            statusCode = error.status;
        }

        if (error.hasOwnProperty('message')) {
            message = error.message;
        }

        if (typeof message == 'string') {
            this.errorMessage = message;
            return;
        }

        if (message instanceof Object) {

            for (let field in message) {

                if (!this.errors.hasOwnProperty(field) || !message.hasOwnProperty(field) || !message[field].hasOwnProperty('message')) {
                    continue;
                }
                let messageText = message[field].message;
                this.errors[field].push(messageText);

            }

            return;
        }

        if (statusCode !== false) {
            this.errorMessage = this.messageService.getMessageFromHttpCode(statusCode as number, this.constructor.name);
        }

    }

    /**
     * Add a new error message specific to a field.
     * 
     * @param {string} field
     * @param {string} message
     */
    protected addFieldError(field: string, message: string) {
        if (!this.errors.hasOwnProperty(field)) {
            throw('Cannot add error. Invalid field: ' + field);
        }
        this.errors[field].push(message);
    }

    /**
     * Set general success message.
     * @param {string} error
     */
    protected setGeneralErrorMessage(error: string) {
        this.resetErrors();
        this.errorMessage = error;
    }

    /**
     * Set general error message.
     * @param {string} message
     */
    protected setGeneralSuccessMessage(message: string) {
        this.resetErrors();
        this.successMessage = message;
    }

}
