import {AuthService} from "../services/auth.service";
import {OnInit} from "@angular/core";

export abstract class UserDependentComponent implements OnInit {

    isLoading: boolean = true;
    isLoggedIn: boolean;

    constructor (protected authService: AuthService) {}

    ngOnInit(): void {

        this.isLoggedIn = this.authService.isLoggedIn();
        if (this.isLoggedIn !== undefined) {
            this.isLoading = false;
        }

        this.authService.getAuthObservable().subscribe(
            (message: any) => {

                this.isLoading = false;

                if (!message.event) {
                    return;
                }

                const event = message.event;

                if (event === AuthService.EVENT_LOG_OUT) {
                    this.isLoggedIn = false;
                } else if (event === AuthService.EVENT_LOG_IN) {
                    this.isLoggedIn = true;
                }

            }
        );

    }

}
