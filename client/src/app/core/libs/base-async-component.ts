import {MessagesService} from "../services/messages.service";
import {Observable} from "rxjs/Rx";
import {OnInit} from "@angular/core/core";

export abstract class BaseAsyncComponent implements OnInit {

    abstract errorCodeMessages: any;

    /**
     * General error message.
     * @type {string}
     */
    errorMessage: string = '';

    isLoading: boolean = true;


    /**
     * Initialise class.
     *
     * @param messageService
     */
    constructor(protected messageService: MessagesService) {}

    /**
     * Initialize.
     */
    ngOnInit(): void {

        this.messageService.addMessages(this.errorCodeMessages, this.constructor.name);

        this.getDataObserver().subscribe(
            (response: any) => {

                this.isLoading = false;
                this.parseResponse(response);

            },

            (error: any) => {

                this.isLoading = false;
                this.handleError(error);

            }
        )

    }

    /**
     * Get the observer for current data in component.
     *
     * @return Observable<any>
     */
    protected abstract getDataObserver(): Observable<any>;


    /**
     * Parse response in case of success.
     */
    protected abstract parseResponse(response: any): void;

    /**
     * Reset messages associated with current form.
     */
    protected resetError() {
        this.errorMessage = '';
    }

    /**
     * Handle errors.
     * @param error
     */
    protected handleError(error: any) {

        this.resetError();

        let statusCode:number|boolean = false;
        let message:boolean|any = false;

        if (error.hasOwnProperty('status')) {
            statusCode = error.status;
        }

        if (error.hasOwnProperty('message')) {
            message = error.message;
        }

        if (typeof message == 'string') {
            this.errorMessage = message;
            return;
        }

        if (statusCode !== false) {
            this.errorMessage = this.messageService.getMessageFromHttpCode(statusCode as number, this.constructor.name);
        }

    }

    /**
     * Send error message.
     *
     * @param message
     */
    protected setErrorMessage(message: string) {

        this.errorMessage = message;

    }

}
