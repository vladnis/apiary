import {Injectable} from '@angular/core';
import 'rxjs/Rx';

@Injectable()
export class MessagesService {

    private defaultMessageMappings = {
        404: 'Service not available',
        0: 'Service not available'
    };

    /**
     * Default message-http code mappings.
     * @type {{}}
     */
    private messageMappings: any = {};

    /**
     * Message used in case no mapping is found.
     * @type {string}
     */
    private undefinedMessage:string = 'Unexpected error occurred';

    /**
     * Initialise service.
     */
    constructor() {
        for (const i in this.defaultMessageMappings) {
            if (!this.defaultMessageMappings.hasOwnProperty(i)) {
                continue;
            }
            this.messageMappings[i] = this.defaultMessageMappings[i];
        }
    }

    /**
     * Register new user.
     *
     * @param code
     * @param context
     * @returns {string|Observable<R>}
     */
    getMessageFromHttpCode(code: number, context: string) {

        if (this.messageMappings.hasOwnProperty(context)) {
            if (this.messageMappings[context].hasOwnProperty(code)) {
                return this.messageMappings[context][code];
            } else {
                return this.undefinedMessage;
            }
        }

        if (this.messageMappings.hasOwnProperty(code)) {
            return this.messageMappings[code];
        }

        return this.undefinedMessage;

    }

    /**
     * Add new code to message mappings.
     * @param messages
     * @param context
     */
    addMessages(messages: any, context: string) {

        if (!this.messageMappings[context]) {
            this.messageMappings[context] = {};
        }

        for (let code in messages) {
            if (!messages.hasOwnProperty(code)) {
                continue;
            }
            this.messageMappings[context][code] = messages[code] as string;
        }

    }

}
