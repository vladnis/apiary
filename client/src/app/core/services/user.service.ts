import {Injectable} from '@angular/core';
import 'rxjs/Rx';
import {CoreHttpService} from "./core-http.service";
import {MessagesService} from "./messages.service";
import {AuthService} from "./auth.service";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService extends CoreHttpService {

    constructor (protected http: HttpClient,  protected messageService: MessagesService, protected authService: AuthService) {
        super(http, messageService);
    }

    /**
     * Get current user information.
     *
     * @returns {Observable<R>}
     */
    getCurrentUserInfo() {

        return this.get('user', {});

    }

    /**
     * Get current user information.
     *
     * @returns {Observable<R>}
     */
    getCurrentUserApiProjects() {

        return this.get('user/api-projects', {});

    }

}
