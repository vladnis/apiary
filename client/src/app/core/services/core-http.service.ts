import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';
import {MessagesService} from "./messages.service";
import {HttpClient, HttpParams} from '@angular/common/http';
import {HttpHeaders} from "@angular/common/http";
import {HttpErrorResponse} from "@angular/common/http";
import {Config} from "../../config/config";

@Injectable()
export class CoreHttpService {

    private host: string = Config.endpointHost;

    constructor (protected http: HttpClient,  protected messageService: MessagesService) {}

    protected extractData(res: any) {

        let body: any = res;

        if (body && body.hasOwnProperty("data") && body.hasOwnProperty("count")) {
            return {
                data: body.data,
                count: body.count,
                status: 200
            };
        }

        return {
            data: body,
            status: 200
        };

    }

    /**
     * Read error from a body response;
     * @param error
     * @returns {any}
     */
    private getErrorFromTheBody(error: any) {

        let response:boolean|any = false;
        try {
            response = error.json();
        } catch(e) {
            return false;
        }

        if (response.hasOwnProperty("errors")) {
            return response.errors;
        }

        if (response.hasOwnProperty("errorMessage")) {
            return response.errorMessage;
        }

        return false;

    }

    /**
     * Handle error from request
     *
     * @param error
     * @returns {any}
     */
    protected handleError(error: any | any) {

        if (error instanceof HttpErrorResponse) {

            // Try to read error from body.
            let errorMsg = this.getErrorFromTheBody(error);

            if (!errorMsg) {
                return Observable.throw({status: error.status});
            }
            return Observable.throw({status: error.status, message: errorMsg});

        }
        return Observable.throw({status: 0});
    }

    /**
     * Send get request
     *
     * @param method
     * @param params
     * @returns {Observable<>}
     */
    public get(method:string, params: any) {

        let httpParams = new HttpParams();

        for (const key in params) {
            if (!params.hasOwnProperty(key)) {
                continue;
            }
            if (typeof params[key] === 'object' && params[key] !== null) {
                params[key] = JSON.stringify(params[key]);
            }
            httpParams = httpParams.set(key, params[key]);
        }
        return this.http.get(this.host + method, { withCredentials: true, params: httpParams })
            .map(this.extractData)
            .catch(this.handleError.bind(this));

    }

    /**
     * Send post request
     *
     * @param method
     * @param params
     * @returns {Observable<any>}
     */
    public post(method:string, params: any) {

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this.http.post(this.host + method, params, { headers: headers, withCredentials: true })
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    }

    /**
     * Send post request
     *
     * @param method
     * @param params
     * @returns {Observable<>}
     */
    public put(method:string, params: any) {

        let options: any = {};

        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        options.headers = headers;
        options.withCredentials = true;

        return this.http.put(this.host + method, params, options)
            .map(this.extractData)
            .catch(this.handleError.bind(this));

    }

    /**
     * Send post request
     *
     * @param method
     * @param params
     * @returns {Observable<>}
     */
    public delete(method:string, params: any) {

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        params.headers = headers;
        params.withCredentials = true;

        return this.http.delete(this.host + method, params )
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    }

    /**
     * Get current host.
     *
     * @returns {string}
     */
    public getHost(): any {
        return this.host;
    }

}
