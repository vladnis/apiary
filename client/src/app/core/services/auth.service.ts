import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {CoreHttpService} from "./core-http.service";
import {MessagesService} from "./messages.service";
import {Subject} from "rxjs/Subject";
import {LoginInfo} from "../../main/login-form/login-info";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService extends CoreHttpService {

    public static EVENT_LOG_IN = 'event_logged_in';
    public static EVENT_LOG_OUT = 'event_logged_out';

    /**
     * Gives events when the user changes.
     */
    private authenticationSubject: Subject<any>;

    private isUserLoggedIn: boolean = undefined;

    /**
     * Initialise service.
     * @param http
     * @param messageService
     */
    constructor (
        protected http: HttpClient,
        protected messageService: MessagesService
    ) {

        super(http, messageService);

        this.createCurrentUserObservable();
        this.requestCurrentUser();

    }

    /**
     * Log in.
     * @param loginInfo
     * @returns {Observable<any>}
     */
    public login(loginInfo: LoginInfo): Observable<boolean> {

        const loginObservable = this.post('auth/login', loginInfo);

        return loginObservable
            .map((result: any) => {

                if (!result || !result.status || result.status !== 200) {
                    return;
                }
                this.isUserLoggedIn = true;
                this.authenticationSubject.next({event: AuthService.EVENT_LOG_IN});

            }).catch((error) => {

                this.authenticationSubject.next({event: AuthService.EVENT_LOG_OUT});
                return Observable.throw(error);

            });

    }

    /**
     * Log out.
     * @returns {Observable<R>}
     */
    public logout(): Observable<any> {

        const logoutObservable = this.post('auth/logout', {});
        return logoutObservable.map(
            (result: any) => {
                if (!result || !result.status || result.status !== 200) {
                    return;
                }

                this.isUserLoggedIn = false;
                this.authenticationSubject.next({event: AuthService.EVENT_LOG_OUT});

            }
        ).catch((error) => {

            return Observable.throw(error);

        });
    }

    /**
     * Get the user that is logged in.
     * @returns {Observable<User>}
     */
    public getAuthObservable() {

        return this.authenticationSubject;

    }

    /**
     * Get current login status.
     * @returns {boolean}
     */
    public isLoggedIn() {

        return this.isUserLoggedIn;

    }

    /**
     * Create subject used for sending events from this service.
     */
    private createCurrentUserObservable() {

        this.authenticationSubject = new Subject();

    }

    /**
     * Request user associated with current session.
     */
    private requestCurrentUser() {

        this.get('auth', {})
            .subscribe(
                (result: any) => {

                    this.isUserLoggedIn = true;
                    this.authenticationSubject.next({event: AuthService.EVENT_LOG_IN});

                },
                (error: any) => {

                    this.isUserLoggedIn = false;
                    this.authenticationSubject.next({event: AuthService.EVENT_LOG_OUT});

                }
            );

    }

}
