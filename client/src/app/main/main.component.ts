import {Component, ViewEncapsulation} from '@angular/core';
import {AuthService} from "../core/services/auth.service";

@Component({
    templateUrl: './main.component.html',
    encapsulation: ViewEncapsulation.None
})
export class MainComponent {

    constructor(private authService: AuthService) { }

}
