import {NgModule} from "@angular/core";
import {MainRoutingModule} from "./main-routing.module";
import {CoreModule} from "../core/core.module";
import {LoginFormComponent} from "./login-form/login-form.component";
import {HeaderComponent} from "./header/header.component";
import {FooterComponent} from "./footer/footer.component";
import {MainComponent} from "./main.component";
import {HomeComponent} from "./home/home.component";
import {UserInfoComponent} from "./user-info/user-info.component";
import {ApiProjectsComponent} from "./api-projects/api-projects.component";

@NgModule({
    imports: [
        CoreModule,
        MainRoutingModule,
    ],
    declarations: [
        LoginFormComponent,
        HeaderComponent,
        FooterComponent,
        MainComponent,
        HomeComponent,
        UserInfoComponent,
        ApiProjectsComponent
    ],
    exports: [
    ],
    providers: [
    ]
})
export class MainModule {}
