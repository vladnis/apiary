import { Component, ViewEncapsulation } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {LoginInfo} from "./login-info";
import {AbstractForm} from "../../core/libs/abstract-form";
import {MessagesService} from "../../core/services/messages.service";
import {Router} from "@angular/router";
import {AuthService} from "../../core/services/auth.service";

@Component({
    selector: 'login-from',
    templateUrl: './login-form.component.html',
    encapsulation: ViewEncapsulation.None
})

export class LoginFormComponent extends AbstractForm {

    errorCodeMessages = {
        200: 'Successfully logged in',
        404: 'User or password are incorrect'
    };

    fields: any = {
        username: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(255)]],
        password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(255)]],
    };

    validationMessages: any = {
        username: {
            'required':      'Username is required',
            'maxlength':     'Value is too large'
        },
        password: {
            'required':      'Password is required',
            'minlength':     'Password must be at least 6 characters long',
            'maxlength':     'Value is too large'
        },
    };

    constructor(public authService: AuthService, protected fb: FormBuilder, protected messageService: MessagesService, private router: Router) {
        super(fb, messageService);
    }

    login() {

        this.setGeneralSuccessMessage('Trying to log in ...');

        const formModel = this.form.value;

        const loginInfo: LoginInfo = {
            username: formModel.username as string,
            password: formModel.password as string,
        };
        this.authService.login(loginInfo).subscribe(
            this.handleSuccess.bind(this),
            this.handleErrors.bind(this)
        );

    }

}
