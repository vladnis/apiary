import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'footer',
    templateUrl: './footer.component.html',
    encapsulation: ViewEncapsulation.None
})
export class FooterComponent {}
