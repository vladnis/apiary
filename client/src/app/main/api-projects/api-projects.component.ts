import { Component, ViewEncapsulation } from '@angular/core';
import {BaseAsyncComponent} from "../../core/libs/base-async-component";
import {MessagesService} from "../../core/services/messages.service";
import {UserService} from "../../core/services/user.service";
import {Observable} from "rxjs/Rx";

@Component({
    selector: 'api-projects',
    templateUrl: './api-projects.component.html',
    encapsulation: ViewEncapsulation.None
})

export class ApiProjectsComponent extends BaseAsyncComponent {

    private apis: any;

    errorCodeMessages = {
        404: 'Cannot find api projects for current user'
    };

    /**
     * Initialise component.
     *
     * @param messageService
     * @param userService
     */
    constructor(protected messageService: MessagesService, private userService: UserService) {

        super(messageService);

    }

    /**
     * Get the observer for current data in component.
     *
     * @return Observable<any>
     */
    protected getDataObserver(): Observable<any> {

        return this.userService.getCurrentUserApiProjects();

    }

    /**
     * Parse response in case of success.
     */
    protected parseResponse(response: any) {

        if (!response.status || response.status !== 200 || !response.data) {
            return this.setErrorMessage("Invalid response");
        }

        const data = response.data;

        if (!data.apis) {
            return this.setErrorMessage("Invalid response");
        }

        this.apis = data.apis;

    }

}
