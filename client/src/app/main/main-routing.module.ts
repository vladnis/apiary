import { NgModule }             from '@angular/core';
import { RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {MainComponent} from "./main.component";

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'main',
                component: MainComponent,
                children: [
                    {
                        path: '',
                        component: HomeComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})

export class MainRoutingModule { }
