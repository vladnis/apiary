import { Component, ViewEncapsulation } from '@angular/core';
import {BaseAsyncComponent} from "../../core/libs/base-async-component";
import {MessagesService} from "../../core/services/messages.service";
import {UserService} from "../../core/services/user.service";
import {Observable} from "rxjs/Rx";

@Component({
    selector: 'user-info',
    templateUrl: './user-info.component.html',
    encapsulation: ViewEncapsulation.None
})

export class UserInfoComponent extends BaseAsyncComponent {

    private userId: string;
    private username: string;

    errorCodeMessages = {
        404: 'Cannot find user'
    };

    /**
     * Initialise component.
     *
     * @param messageService
     * @param userService
     */
    constructor(protected messageService: MessagesService, private userService: UserService) {

        super(messageService);

    }

    /**
     * Get the observer for current data in component.
     *
     * @return Observable<any>
     */
    protected getDataObserver(): Observable<any> {

        return this.userService.getCurrentUserInfo();

    }

    /**
     * Parse response in case of success.
     */
    protected parseResponse(response: any) {

        if (!response.status || response.status !== 200 || !response.data) {
            return this.setErrorMessage("Invalid response");
        }

        const data = response.data;

        if (!data.userId) {
            return this.setErrorMessage("Invalid user id");
        }

        if (!data.userName) {
            return this.setErrorMessage("Invalid user name");
        }

        this.userId = data.userId;
        this.username = data.userName;

    }

}
