import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from "../../core/services/auth.service";
import {UserDependentComponent} from "../../core/libs/user-dependent.component";

@Component({
    templateUrl: './home.component.html',
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent extends UserDependentComponent implements OnInit {

    public constructor(authService: AuthService) {
        super(authService);
    }

    ngOnInit(): void {

        super.ngOnInit();

    }

    logout() {
        this.authService.logout().subscribe();
    }

}
