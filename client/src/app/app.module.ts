import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {NgModule}      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import {CoreModule} from "./core/core.module";
import {AppRoutingModule} from "./app-routing.module";
import {MainModule} from "./main/main.module";


@NgModule({
  imports:      [
      BrowserModule,
      CoreModule,
      MainModule,
      AppRoutingModule,
      BrowserAnimationsModule,
  ],
  declarations: [
      AppComponent
  ],
  bootstrap:    [
      AppComponent
  ]
})
export class AppModule {
}
