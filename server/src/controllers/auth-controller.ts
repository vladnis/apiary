import * as passport from "passport";
import { Request, Response, NextFunction } from "express";
import { AbstractController } from "./core/abstract-controller";
const async = require("async");

export class AuthController extends AbstractController {

    /**
     * POST /login
     * Sign in using email and password.
     */
    login (req: any, res: Response, next: NextFunction) {

        passport.authenticate(
            "local",
            (err: Error, bearier: any, info: any) => {

                if (err) {
                    return next(err);
                }

                if (!bearier) {
                    return res.status(404).json({errorMessage: info.message});
                }

                req.session.bearier = bearier;

                return res.status(200).json({});

            }
        )(req, res, next);

    }

    /**
     * Get currently logged user.
     * @param req
     * @param res
     * @param next
     * @returns {Response}
     */
    getCurrentUser (req: any, res: Response, next: NextFunction) {

        if (!req.session || !req.session.bearier) {
            return res.status(404).json();
        }

        return res.status(200).json({});

    }

    /**
     * Log out current user.
     * @param req
     * @param res
     * @param next
     */
    logout (req: any, res: Response, next: NextFunction) {

        req.session.bearier = undefined;
        return res.status(200).json({});

    }

}
