import { Request, Response, NextFunction } from "express";
import { AbstractController } from "./core/abstract-controller";
import { UserInfoRequester } from "../libs/apiary/UserInfoRequester";
import { UserApiProjectsRequester } from "../libs/apiary/UserApiProjectsRequester";

export class UserController extends AbstractController {

    /**
     * Get current user info.
     *
     * @param req
     * @param res
     * @param next
     */
    get(req: Request, res: Response, next: NextFunction) {

        const userRequester = new UserInfoRequester(req.session.bearier);
        userRequester.getUserInfo((err: any, result: any, user: any) => {

            if (err) {
                return next(err);
            }
            return res.status(200).json(user);

        });

    }

    /**
     * Get current user api projects.
     *
     * @param req
     * @param res
     * @param next
     */
    getApiProjects(req: Request, res: Response, next: NextFunction) {

        const apiRequester = new UserApiProjectsRequester(req.session.bearier);
        apiRequester.getUserApiProjects((err: any, result: any, projects: any) => {

            if (err) {
                return next(err);
            }
            return res.status(200).json(projects);

        });

    }

}
