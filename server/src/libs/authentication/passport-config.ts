/**
 * Created by vlad on 01.06.2017.
 */
import * as passport from "passport";
import { AuthorizationTokenRequester } from "../apiary/AuthorizationTokenRequester";
const passportLocal = require("passport-local").Strategy;

export class PassportConfig {

    constructor() {
        this.configLocalPassport();
    }

    configLocalPassport () {

        const LocalStrategy = passportLocal.Strategy;

        passport.serializeUser<any, any>((user, done) => {
            done(undefined, user);
        });

        passport.deserializeUser((id, done) => {
            done(undefined, id);
        });

        /**
         * Sign in using Email and Password.
         */
        passport.use(new LocalStrategy(
            {
                usernameField: "username",
                passwordField: "password"
            },
            (username: string, password: string, done: any) => {

                const tokenRequester = new AuthorizationTokenRequester(username, password);

                tokenRequester.createToken("test", true, (err: any, token: any) => {

                    if (err) {
                        return done(undefined, false, {message: "Cannot generate authorization token"});
                    }

                    return done(undefined, token);

                });

            })
        );
    }

    getPassport() {
        return passport;
    }

}
