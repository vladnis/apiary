import { NextFunction } from "express";
import { Logger } from "../logger/logger";

export class ExpressAuthMiddleware {

    static isAuthenticated (req: any, res: any, next: NextFunction) {

        if (!req.session || !req.session.bearier) {
            return res.status(401).json({});
        }

        return next();

    }

}
