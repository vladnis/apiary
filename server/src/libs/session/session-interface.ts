/**
 * Created by vlad on 20.05.2017.
 */
import express = require("express");

export interface SessionInterface {

    getMiddleWare(): express.RequestHandler;

}
