import { DatabaseSession } from "./database-session";
import { SessionInterface } from "./session-interface";
/**
 * Created by vlad on 20.05.2017.
 */
export class SessionFactory {

    static getSessionHandler(databaseConnection: any): SessionInterface {

        return new DatabaseSession(databaseConnection);

    }

}
