import * as mongo from "connect-mongo";
import * as session from "express-session";
import express = require("express");
import mongoose = require("mongoose");
import { SessionInterface } from "./session-interface";

export class DatabaseSession implements SessionInterface {

    private sessionMiddleware: express.RequestHandler;

    constructor(databaseConnection: mongoose.Connection) {

        const MongoStore = mongo(session);
        const mongoStore = new MongoStore(
            {
                mongooseConnection: databaseConnection
            }
        );
        this.sessionMiddleware = session(
            {
                resave: false,
                saveUninitialized: true,
                proxy: true,
                secret: "apiary",
                store: mongoStore
            }
        );

    }

    /**
     * Get middleware associated with database session.
     * @returns {e.RequestHandler}
     */
    getMiddleWare(): express.RequestHandler {
        return this.sessionMiddleware;
    }

}
