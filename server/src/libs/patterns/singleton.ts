export class Singleton {

    /**
     * Instance.
     *
     * @type {{}}
     */
    private static instances: any = {};

    /**
     * Get class instance.
     *
     * @param args
     * @return {static}
     */
    public static getInstance(...args: any[]) {
        const className = this.toString().split ("(" || /s+/)[0].split (" " || /s+/)[1];
        if (!Singleton.instances.hasOwnProperty(className)) {
            const instance: any = this;
            Singleton.instances[className] = new instance(...args);
        }

        return Singleton.instances[className];
    }

}
