/**
 * Created by vlad on 14.05.2017.
 */
import { Server } from "../server/server";
import { ConfigParser } from "../config/config-parser";
import { ConfigMap } from "../config/config-map";
import { SessionFactory } from "../session/session-factory";
import { Database } from "../database/database";
import { Logger } from "../logger/logger";
import { PassportConfig } from "../authentication/passport-config";
const async = require("async");

export class Application {

    private server: Server;

    private databaseConnection: any;

    /**
     * Initialise application.
     */
    constructor() {}

    /**
     * Initialise logger.
     */
    private initialiseLogger(callback: any) {

        const loggerConfig = ConfigParser.parseConfig(ConfigMap.loggerConf);
        Logger.init(loggerConfig);

        Logger.info("Initialised logger");

        return callback();

    }

    /**
     * Initialise database.
     *
     * @param {function} callback
     */
    private initialiseDatabase(callback: any) {

        const databaseConfig = ConfigParser.parseConfig(ConfigMap.dbConf);
        const database = new Database(databaseConfig.host);

        database.connect((err: any, connection: any) => {

            if (!err) {
                Logger.info("Initialised database");
                this.databaseConnection = connection;
            }

            return callback(err);

        });
    }

    /**
     * Initialise http server.
     * @param callback
     */
    private initialiseServer(callback: any) {

        const sessionMiddleware = SessionFactory.getSessionHandler(this.databaseConnection).getMiddleWare();
        const serverConfig = ConfigParser.parseConfig(ConfigMap.serverConf);
        const passportConfig: PassportConfig = new PassportConfig();

        this.server = new Server(serverConfig.port , sessionMiddleware, passportConfig.getPassport());
        this.server.start((err: any) => {

            if (!err) {
                Logger.info("Initialised HTTP server");
            }

            return callback(err);

        });

    }

    /**
     * Start app.
     *
     * @param callback
     */
    public start(callback: any) {

        async.waterfall([
            this.initialiseLogger.bind(this),
            this.initialiseDatabase.bind(this),
            this.initialiseServer.bind(this)
        ], (err: any) => {
            if (err) {
                Logger.error(err);
            } else {
                Logger.info("Application stared");
            }

        });

    }

}
