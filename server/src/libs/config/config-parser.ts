import * as fs from "fs";
import { Config } from "./config";

export class ConfigParser {

    /**
     * Get the parsed contents of a config.
     *
     * @param configName
     * @returns {string}
     */
    static parseConfig(configName: string) {
        const content = fs.readFileSync(Config.configFolder + configName, "utf8");
        return JSON.parse(content);

    }

}
