export let ConfigMap = {
    serverConf: "server.json",
    dbConf: "db.json",
    loggerConf: "logger.json",
};
