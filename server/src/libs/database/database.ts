import * as mongoose from "mongoose";

export class Database {
    private host: string;

    constructor(host: string) {
      this.host = host;
    }

    connect(callback: any) {

        mongoose.connect(this.host, {}, function (err) {
            if (err) {
                console.log("MongoDB connection error. Please make sure MongoDB is running.");
                process.exit();
            }
            return callback(err, mongoose.connection);

        });

    }

}
