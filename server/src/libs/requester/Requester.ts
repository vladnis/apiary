const querystring = require("querystring");
const request = require("request");

export abstract class Requester {

    /**
     * Url of API.
     */
    protected abstract url: string;

    /**
     * Send a request to the API.
     *
     * @param params
     * @param method
     * @param customHeader
     * @param callback
     */
    request(params: any, method: string, customHeader: any, callback: any) {

        const paramsUrlEncoded: string = querystring.stringify(params);
        const contentLength: number = paramsUrlEncoded.length;

        const header: any = {
            "Content-Length": contentLength,
            "Content-Type": "application/x-www-form-urlencoded"
        };

        if (header) {

            for (const i in customHeader) {
                if (!customHeader.hasOwnProperty(i)) {
                    continue;
                }
                header[i] = customHeader[i];

            }

        }

        request({
            headers: header,
            uri: this.url,
            body: paramsUrlEncoded,
            method: method
        }, (err: any, res: any, body: any) => {

            if (body) {
                try {
                    body = JSON.parse(body);
                } catch (e) {
                    return callback(e);
                }
            }

            return callback(err, res, body);

        });

    }

    /**
     * Implementation for HTTP GET.
     *
     * @param params
     * @param header
     * @param callback
     * @returns {undefined}
     */
    public get(params: any, header: any, callback: any) {

        return this.request(params, "GET", header, callback);

    }

    /**
     * Implementation for HTTP POST.
     *
     * @param params
     * @param header
     * @param callback
     * @returns {undefined}
     */
    public post(params: any, header: any, callback: any) {

        return this.request(params, "POST", header, callback);

    }

    /**
     * Implementation for HTTP DELETE.
     *
     * @param params
     * @param header
     * @param callback
     * @returns {undefined}
     */
    public delete(params: any, header: any, callback: any) {

        return this.request(params, "DELETE", header, callback);

    }

}
