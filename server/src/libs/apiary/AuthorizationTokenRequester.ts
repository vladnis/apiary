import { Requester } from "../requester/Requester";
import { Token } from "./Token";

/**
 * Used to manage apiary tokens.
 */
export class AuthorizationTokenRequester extends Requester {

    protected url: string = "https://api.apiary.io/authorization";

    private token: Token;

    /**
     * Initialise requester.
     *
     * @param user
     * @param password
     */
    constructor(private user: string, private password: string) {

        super();
        this.token = new Token(user, password);

    }

    /**
     * Create new token.
     *
     * @param tokenDescription
     * @param tokenRegenerate
     * @param callback
     * @returns {undefined}
     */
    public createToken(tokenDescription: string, tokenRegenerate: boolean, callback: any) {

        return this.post({
                tokenDescription: tokenDescription,
                tokenRegenerate: tokenRegenerate
            },
            {
                Authorization: "Basic " + this.token.getCredentials()
            },
            (err: any, res: any, body: any) => {
                if (err) {
                    return callback(err);
                }

                if (!body["token"]) {
                    return callback("Did not receive token");
                }

                return callback(undefined, body["token"]);
            }
        );

    }

    /**
     * Get currently generated tokens.
     *
     * @param callback
     * @returns {undefined}
     */
    public getTokens(callback: any) {

        return this.get(
            {},
            {
                Authorization: "Basic " + this.token.getCredentials()
            },
            callback
        );

    }

    /**
     * Delete token.
     *
     * @param tokenDescription
     * @param callback
     * @returns {undefined}
     */
    public deleteToken(tokenDescription: string, callback: any) {

        return this.delete({
                tokenDescription: tokenDescription
            },
            {
                Authorization: "Basic " + this.token.getCredentials()
            },
            callback
        );

    }

}
