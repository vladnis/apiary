import { Requester } from "../requester/Requester";

/**
 * Used to query information regarding api projects for the logged user.
 */
export class UserApiProjectsRequester extends Requester {

    protected url: string = "https://api.apiary.io/me/apis";

    /**
     * Initialise requester.
     *
     * @param bearer
     */
    constructor(private bearer: string) {

        super();

    }

    /**
     * Get user all prjects.
     *
     * @param callback
     * @returns {undefined}
     */
    getUserApiProjects(callback: any) {

        return this.get({}, {authorization: "bearer " + this.bearer}, callback);

    }

}
