export class Token {

    /**
     * Generated credentials.
     */
    private credentials: string;

    constructor(user: string, password: string) {
        this.credentials = this.getBase64(user + ":" + password);
    }

    /**
     * Hash function.
     *
     * @param str
     * @returns {string}
     */
    private getBase64(str: string): string {
        return new Buffer(str).toString("base64");
    }

    /**
     * Get generated credentials.
     *
     * @returns {string}
     */
    getCredentials() {
        return this.credentials;
    }

}
