import { Requester } from "../requester/Requester";

/**
 * Used for gathering information about current logged user.
 */
export class UserInfoRequester extends Requester {

    protected url: string = "https://api.apiary.io/me";

    /**
     * Initialise requester.
     *
     * @param bearer
     */
    constructor(private bearer: string) {

        super();

    }

    /**
     * Get information about current user.
     *
     * @param callback
     * @returns {undefined}
     */
    getUserInfo(callback: any) {

        return this.get({}, {authorization: "bearer " + this.bearer}, callback);

    }

}
