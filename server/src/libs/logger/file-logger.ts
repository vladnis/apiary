const winston = require("winston");

export class FileLogger implements LoggerInterface {

    constructor(config: any) {
        winston.add(winston.transports.File, {
            filename: config.filePath,
            handleExceptions: true,
            humanReadableUnhandledException: true
        });
        // winston.remove(winston.transports.Console);
        winston.level = config.level;

    }

    /**
     * Log message.
     *
     * @param level
     * @param message
     */
    log(level: string, message: any) {
        winston.log(level, message);

    }


}
