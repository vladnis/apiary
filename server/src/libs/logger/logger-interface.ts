interface LoggerInterface {

    /**
     * Log message.
     *
     * @param level
     * @param message
     */
    log(level: string, message: any): any;

}