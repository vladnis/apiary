import { FileLogger } from "./file-logger";

export class Logger {

    private static logger: LoggerInterface;
    private static config: any;

    static init(config: any) {
        Logger.config = config;
    }

    /**
     * Log message.
     *
     * @param level
     * @param message
     */
    static log(level: string, message: any) {

        if (Logger.config === undefined) {
            throw "Logger was not initialised";
        }

        if (Logger.logger == undefined) {
            Logger.logger = new FileLogger(Logger.config);
        }

        return Logger.logger.log(level, message);

    }

    static error(message: any) {
        Logger.log("error", message);
    }

    static warn(message: any) {
        Logger.log("warn", message);
    }

    static info(message: any) {
        Logger.log("info", message);
    }

    static verbose(message: any) {
        Logger.log("verbose", message);
    }

    static debug(message: any) {
        Logger.log("error", message);
    }

    static silly(message: any) {
        Logger.log("silly", message);
    }

}
