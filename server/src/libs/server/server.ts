/**
 * Created by vlad on 16.05.2017.
 */
import * as express from "express";
import * as compression from "compression";
import * as bodyParser from "body-parser";
import * as morgan from "morgan";
import * as lusca from "lusca";
import expressValidator = require("express-validator");
import { Logger } from "../logger/logger";
import { UserController } from "../../controllers/user-controller";
import { AuthController } from "../../controllers/auth-controller";
import { ExpressAuthMiddleware } from "../authentication/expressAuthMiddleware";
const cors = require("cors");

export class Server {

    private port: any;
    private sessionMiddleWare: any;
    private passport: any;
    private app = express();

    /**
     * Create server on specified port with the specified session middleware.
     *
     * @param port
     * @param sessionMiddleWare
     * @param passport
     */
    constructor(port: number, sessionMiddleWare: any, passport: any) {

        this.port = port;
        this.passport = passport;
        this.sessionMiddleWare = sessionMiddleWare;

        this.configure();
        this.loadServicesRoutes();
        this.loadErrorHandlers();

    }

    /**
     * Setup server.
     */
    configure() {

        const whitelist = ["http://localhost:3000", "http://127.0.0.1"];
        const corsOptions = {
            origin: function (origin: string, callback: any) {

                if (whitelist.indexOf(origin) !== -1) {
                    callback(undefined, true);
                } else {
                    callback(new Error("Not allowed by CORS"));
                }

            },
            credentials: true
        };

        this.app.options("*", cors(corsOptions));
        this.app.use(cors(corsOptions));

        this.app.set("port", this.port);
        this. app.use(compression());
        this.app.use(morgan("dev"));

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));

        this.app.use(expressValidator());
        this.app.use(this.sessionMiddleWare);

        this.app.use(this.passport.initialize());
        this.app.use(this.passport.session());

        this.app.use(lusca.xframe("SAMEORIGIN"));
        this.app.use(lusca.xssProtection(true));
        this.app.use((req, res, next) => {
            res.locals.user = req.user;
            next();
        });

    }

    /**
     * Load service routes.
     */
    loadServicesRoutes() {

        const authController = AuthController.getInstance();
        const userController = UserController.getInstance();

        /* Auth */
        this.app.get("/auth", authController.getCurrentUser.bind(authController));
        this.app.post("/auth/login", authController.login.bind(authController));
        this.app.post("/auth/logout", ExpressAuthMiddleware.isAuthenticated, authController.logout.bind(authController));

        /* User */
        this.app.get("/user", ExpressAuthMiddleware.isAuthenticated, userController.get.bind(userController));
        this.app.get("/user/api-projects", ExpressAuthMiddleware.isAuthenticated, userController.getApiProjects.bind(userController));

    }

    /**
     * Load error handlers.
     */
    loadErrorHandlers() {

        this.app.use(function (err: any, req: any, res: any, next: any) {
            if (res.headersSent) {
                return next(err);
            }
            Logger.error(err);
            res.status(500).json();
        });

    }

    /**
     * Start server.
     *
     * @param callback
     */
    start(callback: any) {

        this.app.listen(
            this.app.get("port"), () => {
                Logger.info("Listening on port: " + this.port);
                return callback();
            }
        );

    }

}
