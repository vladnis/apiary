/**
 * Created by vlad on 10.05.2017.
 */
"use strict";
import { Application } from "./libs/app/app";
import { Logger } from "./libs/logger/logger";

const app = new Application();

app.start(function() {
    Logger.info("Initialised application");
});
