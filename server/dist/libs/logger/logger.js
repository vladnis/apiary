"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const file_logger_1 = require("./file-logger");
class Logger {
    static init(config) {
        Logger.config = config;
    }
    /**
     * Log message.
     *
     * @param level
     * @param message
     */
    static log(level, message) {
        if (Logger.config === undefined) {
            throw "Logger was not initialised";
        }
        if (Logger.logger == undefined) {
            Logger.logger = new file_logger_1.FileLogger(Logger.config);
        }
        return Logger.logger.log(level, message);
    }
    static error(message) {
        Logger.log("error", message);
    }
    static warn(message) {
        Logger.log("warn", message);
    }
    static info(message) {
        Logger.log("info", message);
    }
    static verbose(message) {
        Logger.log("verbose", message);
    }
    static debug(message) {
        Logger.log("error", message);
    }
    static silly(message) {
        Logger.log("silly", message);
    }
}
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map