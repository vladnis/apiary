"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston = require("winston");
class FileLogger {
    constructor(config) {
        winston.add(winston.transports.File, {
            filename: config.filePath,
            handleExceptions: true,
            humanReadableUnhandledException: true
        });
        // winston.remove(winston.transports.Console);
        winston.level = config.level;
    }
    /**
     * Log message.
     *
     * @param level
     * @param message
     */
    log(level, message) {
        winston.log(level, message);
    }
}
exports.FileLogger = FileLogger;
//# sourceMappingURL=file-logger.js.map