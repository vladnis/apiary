"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 27.08.2017.
 */
const AWS = require("aws-sdk");
const logger_1 = require("../../logger/logger");
class S3Upload {
    /**
     * Initialise client.
     *
     * @param config
     * @param callback
     */
    constructor(config, callback) {
        if (!config.accessKeyId || !config.secretAccessKey || !config.bucket) {
            throw new Error("Invalid S3 config");
        }
        this._client = new AWS.S3({
            accessKeyId: config.accessKeyId,
            secretAccessKey: config.secretAccessKey
        });
        this.bucket = config.bucket;
        this.createAppBucket(callback);
    }
    /**
     * Create bucket for upload.
     *
     * @param callback
     */
    createAppBucket(callback) {
        this._client.createBucket({ Bucket: this.bucket }, function (err, data) {
            if (err) {
                logger_1.Logger.error(err);
            }
            if (!callback) {
                return;
            }
            return callback();
        });
    }
    /**
     * Upload object
     *
     * @param key
     * @param obj
     * @param callback
     */
    putObject(key, obj, callback) {
        this._client.putObject({
            Bucket: this.bucket,
            Key: key,
            Body: obj,
            ContentLength: obj.byteCount,
            ACL: "public-read"
        }, function (err, data) {
            callback(err, data);
        });
    }
    /**
     * Delete object
     *
     * @param key
     * @param callback
     */
    deleteObject(key, callback) {
        this._client.deleteObject({
            Bucket: this.bucket,
            Key: key
        }, function (err, data) {
            callback(err, data);
        });
    }
}
exports.S3Upload = S3Upload;
//# sourceMappingURL=S3Upload.js.map