"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 14.05.2017.
 */
const server_1 = require("../server/server");
const config_parser_1 = require("../config/config-parser");
const config_map_1 = require("../config/config-map");
const session_factory_1 = require("../session/session-factory");
const database_1 = require("../database/database");
const logger_1 = require("../logger/logger");
const passport_config_1 = require("../authentication/passport-config");
const async = require("async");
class Application {
    /**
     * Initialise application.
     */
    constructor() { }
    /**
     * Initialise logger.
     */
    initialiseLogger(callback) {
        const loggerConfig = config_parser_1.ConfigParser.parseConfig(config_map_1.ConfigMap.loggerConf);
        logger_1.Logger.init(loggerConfig);
        logger_1.Logger.info("Initialised logger");
        return callback();
    }
    /**
     * Initialise database.
     *
     * @param {function} callback
     */
    initialiseDatabase(callback) {
        const databaseConfig = config_parser_1.ConfigParser.parseConfig(config_map_1.ConfigMap.dbConf);
        const database = new database_1.Database(databaseConfig.host);
        database.connect((err, connection) => {
            if (!err) {
                logger_1.Logger.info("Initialised database");
                this.databaseConnection = connection;
            }
            return callback(err);
        });
    }
    /**
     * Initialise http server.
     * @param callback
     */
    initialiseServer(callback) {
        const sessionMiddleware = session_factory_1.SessionFactory.getSessionHandler(this.databaseConnection).getMiddleWare();
        const serverConfig = config_parser_1.ConfigParser.parseConfig(config_map_1.ConfigMap.serverConf);
        const passportConfig = new passport_config_1.PassportConfig();
        this.server = new server_1.Server(serverConfig.port, sessionMiddleware, passportConfig.getPassport());
        this.server.start((err) => {
            if (!err) {
                logger_1.Logger.info("Initialised HTTP server");
            }
            return callback(err);
        });
    }
    /**
     * Start app.
     *
     * @param callback
     */
    start(callback) {
        async.waterfall([
            this.initialiseLogger.bind(this),
            this.initialiseDatabase.bind(this),
            this.initialiseServer.bind(this)
        ], (err) => {
            if (err) {
                logger_1.Logger.error(err);
            }
            else {
                logger_1.Logger.info("Application stared");
            }
        });
    }
}
exports.Application = Application;
//# sourceMappingURL=app.js.map