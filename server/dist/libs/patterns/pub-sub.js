"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const singleton_1 = require("./singleton");
const events_1 = require("events");
/**
 * Created by vlad on 07.10.2017.
 */
class PubSub extends singleton_1.Singleton {
    /**
     * Initialise constructor.
     */
    constructor() {
        super();
        this.eventEmitter = new events_1.EventEmitter;
    }
    /**
     * Subscribe to queue.
     *
     * @param {string} queueName
     * @param callback
     */
    subscribe(queueName, callback) {
        return this.eventEmitter.on(queueName, callback);
    }
    /**
     * Publish object to queue.
     *
     * @param queueName
     * @param obj
     */
    publish(queueName, ...obj) {
        return this.eventEmitter.emit(queueName, ...obj);
    }
}
exports.PubSub = PubSub;
//# sourceMappingURL=pub-sub.js.map