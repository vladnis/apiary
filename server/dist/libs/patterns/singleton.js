"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Singleton {
    /**
     * Get class instance.
     *
     * @param args
     * @return {static}
     */
    static getInstance(...args) {
        const className = this.toString().split("(" || /s+/)[0].split(" " || /s+/)[1];
        if (!Singleton.instances.hasOwnProperty(className)) {
            const instance = this;
            Singleton.instances[className] = new instance(...args);
        }
        return Singleton.instances[className];
    }
}
/**
 * Instance.
 *
 * @type {{}}
 */
Singleton.instances = {};
exports.Singleton = Singleton;
//# sourceMappingURL=singleton.js.map