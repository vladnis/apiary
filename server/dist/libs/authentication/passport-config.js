"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 01.06.2017.
 */
const passport = require("passport");
const AuthorizationTokenRequester_1 = require("../apiary/AuthorizationTokenRequester");
const passportLocal = require("passport-local").Strategy;
class PassportConfig {
    constructor() {
        this.configLocalPassport();
    }
    configLocalPassport() {
        const LocalStrategy = passportLocal.Strategy;
        passport.serializeUser((user, done) => {
            done(undefined, user);
        });
        passport.deserializeUser((id, done) => {
            done(undefined, id);
        });
        /**
         * Sign in using Email and Password.
         */
        passport.use(new LocalStrategy({
            usernameField: "username",
            passwordField: "password"
        }, (username, password, done) => {
            const tokenRequester = new AuthorizationTokenRequester_1.AuthorizationTokenRequester(username, password);
            tokenRequester.createToken("test", true, (err, token) => {
                if (err) {
                    return done(undefined, false, { message: "Cannot generate authorization token" });
                }
                return done(undefined, token);
            });
        }));
    }
    getPassport() {
        return passport;
    }
}
exports.PassportConfig = PassportConfig;
//# sourceMappingURL=passport-config.js.map