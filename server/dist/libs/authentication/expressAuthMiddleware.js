"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ExpressAuthMiddleware {
    static isAuthenticated(req, res, next) {
        if (!req.session || !req.session.bearier) {
            return res.status(401).json({});
        }
        return next();
    }
}
exports.ExpressAuthMiddleware = ExpressAuthMiddleware;
//# sourceMappingURL=expressAuthMiddleware.js.map