"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../logger/logger");
/**
 *
 * Created by vlad on 09.09.2017.
 */
const socket = require("socket.io");
class WebSocketServer {
    /**
     * Initialise socket server.
     *
     * @param config
     */
    constructor(config) {
        this.numConnection = 0;
        if (!config.port) {
            throw Error("Invalid web socket server config.");
        }
        this.port = config.port;
    }
    /**
     * Start socket server.
     *
     * @param callback
     */
    start(callback) {
        const io = socket(this.port, () => {
        });
        this.server = io;
        io.on("connection", (socket) => {
            this.numConnection++;
            logger_1.Logger.info("Current concurrent connections: " + this.numConnection);
        });
        io.on("disconnect", () => {
            this.numConnection--;
            logger_1.Logger.info("Current concurrent connections: " + this.numConnection);
        });
        return callback(this);
    }
    /**
     * Send a message to all listeners.
     *
     * @param topic
     * @param data
     */
    emitMessage(topic, data) {
        return this.server.emit(topic, data);
    }
}
exports.WebSocketServer = WebSocketServer;
//# sourceMappingURL=websocket-server.js.map