"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Requester_1 = require("../requester/Requester");
/**
 * Used to query information regarding api projects for the logged user.
 */
class UserApiProjectsRequester extends Requester_1.Requester {
    /**
     * Initialise requester.
     *
     * @param bearer
     */
    constructor(bearer) {
        super();
        this.bearer = bearer;
        this.url = "https://api.apiary.io/me/apis";
    }
    /**
     * Get user all prjects.
     *
     * @param callback
     * @returns {undefined}
     */
    getUserApiProjects(callback) {
        return this.get({}, { authorization: "bearer " + this.bearer }, callback);
    }
}
exports.UserApiProjectsRequester = UserApiProjectsRequester;
//# sourceMappingURL=UserApiProjectsRequester.js.map