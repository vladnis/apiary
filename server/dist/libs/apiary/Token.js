"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Token {
    constructor(user, password) {
        this.credentials = this.getBase64(user + ":" + password);
    }
    /**
     * Hash function.
     *
     * @param str
     * @returns {string}
     */
    getBase64(str) {
        return new Buffer(str).toString("base64");
    }
    /**
     * Get generated credentials.
     *
     * @returns {string}
     */
    getCredentials() {
        return this.credentials;
    }
}
exports.Token = Token;
//# sourceMappingURL=Token.js.map