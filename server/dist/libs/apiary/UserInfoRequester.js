"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Requester_1 = require("../requester/Requester");
/**
 * Used for gathering information about current logged user.
 */
class UserInfoRequester extends Requester_1.Requester {
    /**
     * Initialise requester.
     *
     * @param bearer
     */
    constructor(bearer) {
        super();
        this.bearer = bearer;
        this.url = "https://api.apiary.io/me";
    }
    /**
     * Get information about current user.
     *
     * @param callback
     * @returns {undefined}
     */
    getUserInfo(callback) {
        return this.get({}, { authorization: "bearer " + this.bearer }, callback);
    }
}
exports.UserInfoRequester = UserInfoRequester;
//# sourceMappingURL=UserInfoRequester.js.map