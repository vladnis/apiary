"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Requester_1 = require("../requester/Requester");
const Token_1 = require("./Token");
/**
 * Used to manage apiary tokens.
 */
class AuthorizationTokenRequester extends Requester_1.Requester {
    /**
     * Initialise requester.
     *
     * @param user
     * @param password
     */
    constructor(user, password) {
        super();
        this.user = user;
        this.password = password;
        this.url = "https://api.apiary.io/authorization";
        this.token = new Token_1.Token(user, password);
    }
    /**
     * Create new token.
     *
     * @param tokenDescription
     * @param tokenRegenerate
     * @param callback
     * @returns {undefined}
     */
    createToken(tokenDescription, tokenRegenerate, callback) {
        return this.post({
            tokenDescription: tokenDescription,
            tokenRegenerate: tokenRegenerate
        }, {
            Authorization: "Basic " + this.token.getCredentials()
        }, (err, res, body) => {
            if (err) {
                return callback(err);
            }
            if (!body["token"]) {
                return callback("Did not receive token");
            }
            return callback(undefined, body["token"]);
        });
    }
    /**
     * Get currently generated tokens.
     *
     * @param callback
     * @returns {undefined}
     */
    getTokens(callback) {
        return this.get({}, {
            Authorization: "Basic " + this.token.getCredentials()
        }, callback);
    }
    /**
     * Delete token.
     *
     * @param tokenDescription
     * @param callback
     * @returns {undefined}
     */
    deleteToken(tokenDescription, callback) {
        return this.delete({
            tokenDescription: tokenDescription
        }, {
            Authorization: "Basic " + this.token.getCredentials()
        }, callback);
    }
}
exports.AuthorizationTokenRequester = AuthorizationTokenRequester;
//# sourceMappingURL=AuthorizationTokenRequester.js.map