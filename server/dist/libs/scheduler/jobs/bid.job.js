"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../../logger/logger");
const auction_controller_1 = require("../../../controllers/auction-controller");
const auction_1 = require("../../../models/auction/auction");
const auto_bid_controller_1 = require("../../../controllers/auto-bid-controller");
class BidJob {
    /**
     * Check autobid.
     * @param job
     * @param done
     * @return {any}
     */
    checkAutoBidJob(job, done) {
        const data = job.attrs.data;
        if (!data.auctionId) {
            logger_1.Logger.error("Bid Job failed. Invalid auction id.");
            return done();
        }
        auction_1.default.findById(data.auctionId, (err, auction) => {
            if (err || !auction) {
                logger_1.Logger.error("Bid Job failed. Invalid auction.");
                return done();
            }
            const auctionController = auction_controller_1.AuctionController.getInstance();
            auctionController.checkAutoBid(auction);
            done();
        });
    }
    /**
     * Check autobid.
     * @param job
     * @param done
     * @return {any}
     */
    checkBidUserAutoBidJob(job, done) {
        const autoBidController = auto_bid_controller_1.AutoBidController.getInstance();
        autoBidController.checkAutoBid(job.attrs.data);
        done();
    }
}
exports.BidJob = BidJob;
//# sourceMappingURL=bid.job.js.map