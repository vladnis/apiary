"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Agenda = require("agenda");
const fs = require("fs");
const bid_job_1 = require("./jobs/bid.job");
/**
 * Created by vlad on 24.09.2017.
 */
class Scheduler {
    /**
     * Initialise scheduler.
     *
     * @param config
     * @param callback
     */
    constructor(config, callback) {
        /**
         * Jobs folder.
         * @type {string}
         */
        this.jobsFolder = "./jobs/";
        this.jobClasses = [
            bid_job_1.BidJob
        ];
        if (!config.connection) {
            throw Error("Invalid scheduler server config.");
        }
        this.scheduler = new Agenda({ db: { address: config.connection } });
        this.scheduler.on("ready", (err) => {
            this.scheduler.start();
            return callback(err, this);
        });
        this._registerJobs();
    }
    /**
     * Get all methods that are jobs from class.
     *
     * @param classObj
     * @return {string[]}
     */
    getAllJobsFromClass(classObj) {
        return Object.getOwnPropertyNames(Object.getPrototypeOf(classObj)).filter((e) => {
            if (typeof classObj[e] == "function" && e.endsWith("Job")) {
                return true;
            }
        });
    }
    /**
     * Register jobs.
     * @private
     */
    _registerJobs() {
        this.jobClasses.forEach((className) => {
            const classObj = new className();
            this.getAllJobsFromClass(classObj).forEach((method) => {
                this.scheduler.define(method, { lockLifetime: 10000 }, classObj[method].bind(classObj));
            });
        });
    }
    /**
     * Create a scheduled job.
     *
     * @param name
     * @param params
     * @param interval
     * @param done
     */
    createScheduledJob(name, params, interval, done) {
        this.scheduler.schedule(interval, name, params, (err) => {
            return done(err);
        });
    }
    /**
     * Create a recurent job.
     *
     * @param name
     * @param params
     * @param interval
     * @param done
     */
    createRecurrentJob(name, params, interval, done) {
        this.scheduler.every(interval, name, params, (err) => {
            return done(err);
        });
    }
    /**
     * Delete a job by name.
     * @param name
     * @param done
     */
    deleteJob(name, done) {
        this.scheduler.cancel({ name: name }, function (err, numRemoved) {
            return done(err, numRemoved);
        });
    }
}
exports.Scheduler = Scheduler;
//# sourceMappingURL=scheduler.js.map