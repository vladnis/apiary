"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const querystring = require("querystring");
const request = require("request");
class Requester {
    /**
     * Send a request to the API.
     *
     * @param params
     * @param method
     * @param customHeader
     * @param callback
     */
    request(params, method, customHeader, callback) {
        const paramsUrlEncoded = querystring.stringify(params);
        const contentLength = paramsUrlEncoded.length;
        const header = {
            "Content-Length": contentLength,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        if (header) {
            for (const i in customHeader) {
                if (!customHeader.hasOwnProperty(i)) {
                    continue;
                }
                header[i] = customHeader[i];
            }
        }
        request({
            headers: header,
            uri: this.url,
            body: paramsUrlEncoded,
            method: method
        }, (err, res, body) => {
            if (body) {
                try {
                    body = JSON.parse(body);
                }
                catch (e) {
                    return callback(e);
                }
            }
            return callback(err, res, body);
        });
    }
    /**
     * Implementation for HTTP GET.
     *
     * @param params
     * @param header
     * @param callback
     * @returns {undefined}
     */
    get(params, header, callback) {
        return this.request(params, "GET", header, callback);
    }
    /**
     * Implementation for HTTP POST.
     *
     * @param params
     * @param header
     * @param callback
     * @returns {undefined}
     */
    post(params, header, callback) {
        return this.request(params, "POST", header, callback);
    }
    /**
     * Implementation for HTTP DELETE.
     *
     * @param params
     * @param header
     * @param callback
     * @returns {undefined}
     */
    delete(params, header, callback) {
        return this.request(params, "DELETE", header, callback);
    }
}
exports.Requester = Requester;
//# sourceMappingURL=Requester.js.map