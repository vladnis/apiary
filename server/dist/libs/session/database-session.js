"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongo = require("connect-mongo");
const session = require("express-session");
class DatabaseSession {
    constructor(databaseConnection) {
        const MongoStore = mongo(session);
        const mongoStore = new MongoStore({
            mongooseConnection: databaseConnection
        });
        this.sessionMiddleware = session({
            resave: false,
            saveUninitialized: true,
            proxy: true,
            secret: "apiary",
            store: mongoStore
        });
    }
    /**
     * Get middleware associated with database session.
     * @returns {e.RequestHandler}
     */
    getMiddleWare() {
        return this.sessionMiddleware;
    }
}
exports.DatabaseSession = DatabaseSession;
//# sourceMappingURL=database-session.js.map