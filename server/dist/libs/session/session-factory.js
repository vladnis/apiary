"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database_session_1 = require("./database-session");
/**
 * Created by vlad on 20.05.2017.
 */
class SessionFactory {
    static getSessionHandler(databaseConnection) {
        return new database_session_1.DatabaseSession(databaseConnection);
    }
}
exports.SessionFactory = SessionFactory;
//# sourceMappingURL=session-factory.js.map