/**
 * Created by vlad on 14.05.2017.
 */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin_user_1 = require("../../models/user/admin-user");
const singleton_1 = require("../patterns/singleton");
const permission_1 = require("../../models/user/permission");
const app_settings_1 = require("../../models/app-settings/app-settings");
const logger_1 = require("../logger/logger");
const languages_1 = require("../../models/languages/languages");
const async = require("async");
class Install extends singleton_1.Singleton {
    /**
     * Start install.
     *
     * @param callback
     */
    start(callback) {
        async.waterfall([
            this.checkIfAlreadyInstalled,
            this.insertAdminUser,
            this.insertLanguages,
            this.selectDefaultLanguage,
            this.finishInstall
        ], function (err, result) {
            if (err && !err.hasOwnProperty("installed") && err.installed !== false) {
                return callback(err);
            }
            return callback();
        });
    }
    /**
     * Check if application is installed and if not create default settings.
     * @param callback
     */
    checkIfAlreadyInstalled(callback) {
        app_settings_1.default.findOneAndUpdate({}, { $setOnInsert: { installed: false } }, { upsert: true }, function (err, doc) {
            if (err) {
                return callback(err);
            }
            if (doc && doc.installed === true) {
                logger_1.Logger.debug("Application already installed");
                return callback({ installed: true });
            }
            logger_1.Logger.debug("Starting install");
            callback();
        });
    }
    /**
     * Insert supported translations.
     * @param callback
     */
    insertLanguages(callback) {
        languages_1.default.insertMany([
            {
                filePath: "assets/images/flags/us.png",
                name: "English"
            },
            {
                filePath: "assets/images/flags/fr.png",
                name: "French"
            },
            {
                filePath: "assets/images/flags/de.png",
                name: "German"
            },
            {
                filePath: "assets/images/flags/es.png",
                name: "Spanish"
            },
            {
                filePath: "assets/images/flags/nl.png",
                name: "Dutch"
            },
            {
                filePath: "assets/images/flags/it.png",
                name: "Italian"
            },
            {
                filePath: "assets/images/flags/br.png",
                name: "Portuguese"
            },
            {
                filePath: "assets/images/flags/ru.png",
                name: "Russian"
            },
            {
                filePath: "assets/images/flags/jp.png",
                name: "Japanese"
            },
            {
                filePath: "assets/images/flags/cn.png",
                name: "Chinese"
            },
            {
                filePath: "assets/images/flags/se.png",
                name: "Swedish"
            },
        ]).then((value => {
            logger_1.Logger.debug("Successfully inserted languages");
            callback();
        })).catch(err => {
            logger_1.Logger.error("Cannot insert languages");
            callback(err);
        });
    }
    /**
     * Select default languages.
     * @param callback
     */
    selectDefaultLanguage(callback) {
        // By default only english is available
        languages_1.default.findOne({ name: "English" }).then(val => {
            app_settings_1.default.findOneAndUpdate({ installed: false }, {
                languages: [val._id],
                auctionExtend: {
                    from: 1,
                    to: 10
                },
                currency: {
                    defaultCurrency: 1,
                    moneyFormat: 1,
                    moneyDecimals: 2,
                    moneySymbolPosition: 0,
                },
                initialBidsNumber: 2,
                aboutUsPage: {
                    enabled: false,
                    description: ""
                },
                howItWorks: {
                    enabled: false,
                    description: ""
                },
                termsAndConditions: {
                    enabled: false,
                    description: ""
                },
                faq: {
                    enabled: false,
                    description: ""
                },
                acceptanceText: {
                    enabled: false,
                    description: ""
                },
                news: {
                    enabled: false
                },
                dateFormat: 0,
                auctions: {
                    showClosedAuctions: true,
                    noAuctionsDisplayed: 10
                },
                batchSettings: {
                    deleteAuctionsOlderThan: 30
                }
            }).then(value => {
                logger_1.Logger.debug("Inserted default language.");
                callback();
            }).catch(err => {
                logger_1.Logger.error("Cannot insert default language.");
                callback(err);
            });
        }).catch(err => {
            return callback(err);
        });
    }
    /**
     * Finish install.
     * @param callback
     */
    finishInstall(callback) {
        app_settings_1.default.findOneAndUpdate({ installed: false }, { installed: true }).then(value => {
            logger_1.Logger.debug("Finished install.");
            callback();
        }).catch(err => {
            logger_1.Logger.error("Cannot finish install.");
            callback(err);
        });
    }
    /**
     * Install admin default user.
     *
     * @param callback
     */
    insertAdminUser(callback) {
        const admin = new admin_user_1.default({
            username: "admin",
            password: "default",
            passwordConfirmation: "default",
            email: "modify@test.com",
            confirmEmail: "modify@test.com",
            permissions: [],
            activated: true
        });
        const adminPermission = new permission_1.default({
            name: permission_1.Permissions.PERMISSION_ADMIN,
        });
        admin.save((err) => {
            if (err && err.code !== 11000) {
                throw err;
            }
            // The admin use already exists
            if (err && err.code === 11000) {
                return callback();
            }
            adminPermission.save((err) => {
                if (err && err.code !== 11000) {
                    throw err;
                }
                admin.update({ permissions: [adminPermission] }, (err) => {
                    logger_1.Logger.debug("Inserted admin user");
                    return callback();
                });
            });
        });
    }
}
exports.Install = Install;
//# sourceMappingURL=install.js.map