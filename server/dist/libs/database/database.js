"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
class Database {
    constructor(host) {
        this.host = host;
    }
    connect(callback) {
        mongoose.connect(this.host, {}, function (err) {
            if (err) {
                console.log("MongoDB connection error. Please make sure MongoDB is running.");
                process.exit();
            }
            return callback(err, mongoose.connection);
        });
    }
}
exports.Database = Database;
//# sourceMappingURL=database.js.map