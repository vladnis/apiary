"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const config_1 = require("./config");
class ConfigParser {
    /**
     * Get the parsed contents of a config.
     *
     * @param configName
     * @returns {string}
     */
    static parseConfig(configName) {
        const content = fs.readFileSync(config_1.Config.configFolder + configName, "utf8");
        return JSON.parse(content);
    }
}
exports.ConfigParser = ConfigParser;
//# sourceMappingURL=config-parser.js.map