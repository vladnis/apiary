"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 16.05.2017.
 */
const express = require("express");
const compression = require("compression");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const lusca = require("lusca");
const expressValidator = require("express-validator");
const logger_1 = require("../logger/logger");
const user_controller_1 = require("../../controllers/user-controller");
const auth_controller_1 = require("../../controllers/auth-controller");
const expressAuthMiddleware_1 = require("../authentication/expressAuthMiddleware");
const cors = require("cors");
class Server {
    /**
     * Create server on specified port with the specified session middleware.
     *
     * @param port
     * @param sessionMiddleWare
     * @param passport
     */
    constructor(port, sessionMiddleWare, passport) {
        this.app = express();
        this.port = port;
        this.passport = passport;
        this.sessionMiddleWare = sessionMiddleWare;
        this.configure();
        this.loadServicesRoutes();
        this.loadErrorHandlers();
    }
    /**
     * Setup server.
     */
    configure() {
        const whitelist = ["http://localhost:3000", "http://127.0.0.1"];
        const corsOptions = {
            origin: function (origin, callback) {
                if (whitelist.indexOf(origin) !== -1) {
                    callback(undefined, true);
                }
                else {
                    callback(new Error("Not allowed by CORS"));
                }
            },
            credentials: true
        };
        this.app.options("*", cors(corsOptions));
        this.app.use(cors(corsOptions));
        this.app.set("port", this.port);
        this.app.use(compression());
        this.app.use(morgan("dev"));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(expressValidator());
        this.app.use(this.sessionMiddleWare);
        this.app.use(this.passport.initialize());
        this.app.use(this.passport.session());
        this.app.use(lusca.xframe("SAMEORIGIN"));
        this.app.use(lusca.xssProtection(true));
        this.app.use((req, res, next) => {
            res.locals.user = req.user;
            next();
        });
    }
    /**
     * Load service routes.
     */
    loadServicesRoutes() {
        const authController = auth_controller_1.AuthController.getInstance();
        const userController = user_controller_1.UserController.getInstance();
        /* Auth */
        this.app.get("/auth", authController.getCurrentUser.bind(authController));
        this.app.post("/auth/login", authController.login.bind(authController));
        this.app.post("/auth/logout", expressAuthMiddleware_1.ExpressAuthMiddleware.isAuthenticated, authController.logout.bind(authController));
        /* User */
        this.app.get("/user", expressAuthMiddleware_1.ExpressAuthMiddleware.isAuthenticated, userController.get.bind(userController));
        this.app.get("/user/api-projects", expressAuthMiddleware_1.ExpressAuthMiddleware.isAuthenticated, userController.getApiProjects.bind(userController));
    }
    /**
     * Load error handlers.
     */
    loadErrorHandlers() {
        this.app.use(function (err, req, res, next) {
            if (res.headersSent) {
                return next(err);
            }
            logger_1.Logger.error(err);
            res.status(500).json();
        });
    }
    /**
     * Start server.
     *
     * @param callback
     */
    start(callback) {
        this.app.listen(this.app.get("port"), () => {
            logger_1.Logger.info("Listening on port: " + this.port);
            return callback();
        });
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map