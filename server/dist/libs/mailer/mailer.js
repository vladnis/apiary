"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodeMailer = require("nodemailer");
/**
 * Created by vlad on 17.06.2017.
 */
class Mailer {
    constructor(config, transport) {
        this.config = config;
        if (!this.config.from) {
            throw new Error("Invalid mail config. Missing form key.");
        }
        this.transporter = nodeMailer.createTransport(transport.getTransport());
    }
    sendMail(to, template, done) {
        const mailOptions = {
            to: to,
            from: this.config.from,
            subject: template.getSubject(),
            text: template.getContent()
        };
        this.transporter.sendMail(mailOptions, (err) => {
            done(err);
        });
    }
}
exports.Mailer = Mailer;
//# sourceMappingURL=mailer.js.map