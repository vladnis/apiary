"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 18.06.2017.
 */
const ses_transport_1 = require("./ses-transport");
class MailTransportFactory {
    static getTransport(config) {
        if (!config.type) {
            throw Error("No email type specified in mail transport factory");
        }
        if (!this.typeMapper[config.type]) {
            throw Error("Invalid mail transport type");
        }
        const name = this.typeMapper[config.type];
        const transportInstance = new name(config);
        if (transportInstance === undefined) {
            throw Error("Invalid transport");
        }
        return transportInstance;
    }
}
MailTransportFactory.typeMapper = {
    "ses": ses_transport_1.SesTransport
};
exports.MailTransportFactory = MailTransportFactory;
//# sourceMappingURL=mail-transport-factory.js.map