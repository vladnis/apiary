"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 18.06.2017.
 */
const ses = require("nodemailer-ses-transport");
class SesTransport {
    constructor(config) {
        this.config = config;
    }
    getTransport() {
        return ses({
            accessKeyId: this.config.AMAZON_KEY,
            secretAccessKey: this.config.AMAZON_SECRET_KEY,
            region: this.config.region
        });
    }
}
exports.SesTransport = SesTransport;
//# sourceMappingURL=ses-transport.js.map