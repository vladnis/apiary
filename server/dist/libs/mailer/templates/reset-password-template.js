"use strict";
/**
 * Created by vlad on 17.06.2017.
 */
Object.defineProperty(exports, "__esModule", { value: true });
class ResetPasswordTemplate {
    constructor(params) {
        this.params = params;
    }
    getContent() {
        return `You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n
                  Please click on the following link, or paste this into your browser to complete the process:\n\n
                  ${this.params.host}/main/reset/${this.params.token}\n\n
                  If you did not request this, please ignore this email and your password will remain unchanged.\n`;
    }
    getSubject() {
        return "Reset your password";
    }
}
exports.ResetPasswordTemplate = ResetPasswordTemplate;
//# sourceMappingURL=reset-password-template.js.map