"use strict";
/**
 * Created by vlad on 17.06.2017.
 */
Object.defineProperty(exports, "__esModule", { value: true });
class MailTemplateFactory {
    static getTemplate(name, params) {
        return new name(params);
    }
}
exports.MailTemplateFactory = MailTemplateFactory;
//# sourceMappingURL=mail-template-factory.js.map