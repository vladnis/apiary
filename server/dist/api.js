/**
 * Created by vlad on 10.05.2017.
 */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("./libs/app/app");
const logger_1 = require("./libs/logger/logger");
const app = new app_1.Application();
app.start(function () {
    logger_1.Logger.info("Initialised application");
});
//# sourceMappingURL=api.js.map