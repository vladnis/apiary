"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const abstract_controller_1 = require("./core/abstract-controller");
const UserInfoRequester_1 = require("../libs/apiary/UserInfoRequester");
const UserApiProjectsRequester_1 = require("../libs/apiary/UserApiProjectsRequester");
class UserController extends abstract_controller_1.AbstractController {
    /**
     * Get current user info.
     *
     * @param req
     * @param res
     * @param next
     */
    get(req, res, next) {
        const userRequester = new UserInfoRequester_1.UserInfoRequester(req.session.bearier);
        userRequester.getUserInfo((err, result, user) => {
            if (err) {
                return next(err);
            }
            return res.status(200).json(user);
        });
    }
    /**
     * Get current user api projects.
     *
     * @param req
     * @param res
     * @param next
     */
    getApiProjects(req, res, next) {
        const apiRequester = new UserApiProjectsRequester_1.UserApiProjectsRequester(req.session.bearier);
        apiRequester.getUserApiProjects((err, result, projects) => {
            if (err) {
                return next(err);
            }
            return res.status(200).json(projects);
        });
    }
}
exports.UserController = UserController;
//# sourceMappingURL=user-controller.js.map