"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passport = require("passport");
const abstract_controller_1 = require("./core/abstract-controller");
const async = require("async");
class AuthController extends abstract_controller_1.AbstractController {
    /**
     * POST /login
     * Sign in using email and password.
     */
    login(req, res, next) {
        passport.authenticate("local", (err, bearier, info) => {
            if (err) {
                return next(err);
            }
            if (!bearier) {
                return res.status(404).json({ errorMessage: info.message });
            }
            req.session.bearier = bearier;
            return res.status(200).json({});
        })(req, res, next);
    }
    /**
     * Get currently logged user.
     * @param req
     * @param res
     * @param next
     * @returns {Response}
     */
    getCurrentUser(req, res, next) {
        if (!req.session || !req.session.bearier) {
            return res.status(404).json();
        }
        return res.status(200).json({});
    }
    /**
     * Log out current user.
     * @param req
     * @param res
     * @param next
     */
    logout(req, res, next) {
        req.session.bearier = undefined;
        return res.status(200).json({});
    }
}
exports.AuthController = AuthController;
//# sourceMappingURL=auth-controller.js.map