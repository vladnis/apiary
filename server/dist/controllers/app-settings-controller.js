"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const abstract_controller_1 = require("./core/abstract-controller");
const app_settings_1 = require("../models/app-settings/app-settings");
const languages_1 = require("../models/languages/languages");
class AppSettingsController extends abstract_controller_1.AbstractController {
    /**
     * Get application settings.
     * @get
     */
    getSettings(req, res, next) {
        app_settings_1.default.findOne({ installed: true }).populate("languages").exec((err, settings) => {
            if (err) {
                return next(err);
            }
            res.status(200).json(settings);
        });
    }
    /**
     * Set application settings.
     * @post
     */
    setSettings(req, res, next) {
        const update = {};
        if (req.body.languages) {
            const languageIds = req.body.languages;
            for (const i in req.body.languages) {
                if (!req.body.languages.hasOwnProperty(i)) {
                    continue;
                }
                req.sanitizeBody("languages" + "." + i).toString();
            }
            languages_1.default.find({ "_id": { "$in": languageIds } }).then((languages) => {
                update.languages = languages.map((language) => {
                    return language._id;
                });
                return app_settings_1.default.findOneAndUpdate({ installed: true }, { "$set": update }).then((value) => {
                    return this.getSettings(req, res, next);
                }).catch((err) => {
                    return next(err);
                });
            }).catch((err) => {
                return next(err);
            });
        }
        if (req.body.auctionExtend) {
            req.sanitizeBody("auctionExtend.to").toString();
            req.sanitizeBody("auctionExtend.from").toString();
            update.auctionExtend = {
                to: req.body.auctionExtend.to,
                from: req.body.auctionExtend.from,
            };
        }
        if (req.body.initialBidsNumber) {
            req.sanitizeBody("initialBidsNumber").toString();
            update.initialBidsNumber = req.body.initialBidsNumber;
        }
        if (req.body.aboutUsPage
            && req.body.aboutUsPage.hasOwnProperty("enabled")
            && req.body.aboutUsPage.hasOwnProperty("description")) {
            update.aboutUsPage = {
                enabled: req.body.aboutUsPage.enabled,
                description: req.body.aboutUsPage.description
            };
        }
        if (req.body.howItWorks
            && req.body.howItWorks.hasOwnProperty("enabled")
            && req.body.howItWorks.hasOwnProperty("description")) {
            update.howItWorks = {
                enabled: req.body.howItWorks.enabled,
                description: req.body.howItWorks.description
            };
        }
        if (req.body.termsAndConditions
            && req.body.termsAndConditions.hasOwnProperty("enabled")
            && req.body.termsAndConditions.hasOwnProperty("description")) {
            update.termsAndConditions = {
                enabled: req.body.termsAndConditions.enabled,
                description: req.body.termsAndConditions.description
            };
        }
        if (req.body.faq
            && req.body.faq.hasOwnProperty("enabled")
            && req.body.faq.hasOwnProperty("description")) {
            update.faq = {
                enabled: req.body.faq.enabled,
                description: req.body.faq.description
            };
        }
        if (req.body.acceptanceText
            && req.body.acceptanceText.hasOwnProperty("enabled")
            && req.body.acceptanceText.hasOwnProperty("description")) {
            update.acceptanceText = {
                enabled: req.body.acceptanceText.enabled,
                description: req.body.acceptanceText.description
            };
        }
        if (req.body.news
            && req.body.news.hasOwnProperty("enabled")) {
            update.news = {
                enabled: req.body.news.enabled,
            };
        }
        if (req.body.dateFormat) {
            update.dateFormat = req.body.dateFormat;
        }
        if (req.body.currency
            && req.body.currency.hasOwnProperty("defaultCurrency")
            && req.body.currency.hasOwnProperty("moneyFormat")
            && req.body.currency.hasOwnProperty("moneyDecimals")
            && req.body.currency.hasOwnProperty("moneySymbolPosition")) {
            update.currency = {
                defaultCurrency: req.body.currency.defaultCurrency,
                moneyFormat: req.body.currency.moneyFormat,
                moneyDecimals: req.body.currency.moneyDecimals,
                moneySymbolPosition: req.body.currency.moneySymbolPosition,
            };
        }
        if (req.body.auctions
            && req.body.auctions.hasOwnProperty("showClosedAuctions")
            && req.body.auctions.hasOwnProperty("noAuctionsDisplayed")) {
            update.auctions = {
                showClosedAuctions: req.body.auctions.showClosedAuctions,
                noAuctionsDisplayed: req.body.auctions.noAuctionsDisplayed,
            };
        }
        if (req.body.batchSettings
            && req.body.batchSettings.hasOwnProperty("deleteAuctionsOlderThan")) {
            update.batchSettings = {
                deleteAuctionsOlderThan: req.body.batchSettings.deleteAuctionsOlderThan
            };
        }
        if (!update) {
            return res.status(200).json();
        }
        app_settings_1.default.findOneAndUpdate({ installed: true }, { "$set": update }).then((value) => {
            return this.getSettings(req, res, next);
        }).catch((err) => {
            return next(err);
        });
    }
    /**
     * Get all available languages
     * @param req
     * @param res
     * @param next
     */
    getLanguages(req, res, next) {
        languages_1.default.find().then((languages) => {
            return res.status(200).json(languages);
        }).catch((err) => {
            return next(err);
        });
    }
}
exports.AppSettingsController = AppSettingsController;
//# sourceMappingURL=app-settings-controller.js.map