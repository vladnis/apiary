"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const abstract_controller_1 = require("./abstract-controller");
const pub_sub_1 = require("../../libs/patterns/pub-sub");
/**
 * Created by vlad on 28.08.2017.
 */
class CoreCrudController extends abstract_controller_1.AbstractController {
    constructor() {
        super(...arguments);
        /**
         * Add created at field.
         *
         * @type {boolean}
         */
        this.addCreatedAt = true;
        /**
         * Add user id when the item is created?
         *
         * @type {boolean}
         */
        this.addUserId = false;
    }
    /**
     * Get all items.
     *
     * @param req
     * @param res
     * @param next
     */
    getItems(req, res, next) {
        this.resource.count({}, (err, count) => {
            if (err) {
                return next(err);
            }
            if (!req.hasOwnProperty("tableFilter")) {
                req.tableFilter = {};
            }
            this.resource.find(req.tableFilter, this.selectFields, req.tableOptions, (err, items) => {
                if (err) {
                    return next(err);
                }
                res.status(200).json({
                    data: items,
                    count: count
                });
            });
        });
    }
    /**
     * Get all items.
     *
     * @param req
     * @param res
     * @param next
     */
    getItem(req, res, next) {
        if (!req.params.id) {
            return res.status(401).json();
        }
        this.resource.findById(req.params.id, (err, item) => {
            if (err) {
                return next(err);
            }
            if (!item) {
                return res.status(404).json();
            }
            return res.status(200).json(item);
        });
    }
    /**
     * Delete item.
     *
     * @param req
     * @param res
     * @param next
     */
    deleteItem(req, res, next) {
        if (!req.params.id) {
            return res.status(401).json();
        }
        this.resource.findByIdAndRemove(req.params.id, (err, item) => {
            if (err) {
                return next(err);
            }
            if (!item) {
                return res.status(404).json();
            }
            return res.status(200).json(item);
        });
    }
    /**
     * Add new item.
     *
     * @param req
     * @param res
     * @param next
     */
    createItem(req, res, next) {
        if (this.addCreatedAt) {
            req.body.createdAt = new Date();
        }
        if (this.addUserId) {
            req.body.userId = req.user._id;
        }
        req.body = this.prepareRecordForCreate(req.body);
        const newItem = new this.resource(req.body);
        newItem.save((err, item) => {
            if (err) {
                return next(err);
            }
            if (this.pubSubTag && typeof this.pubSubTag === "string") {
                pub_sub_1.PubSub.getInstance().publish("/" + this.pubSubTag + "/create", item);
            }
            return res.status(200).json();
        });
    }
    /**
     * Prepare an item to be created.
     * @param record
     * @returns {any}
     */
    prepareRecordForCreate(record) {
        return record;
    }
    /**
     * Add new item
     *
     * @param req
     * @param res
     * @param next
     */
    updateItem(req, res, next) {
        if (!req.params.id) {
            return res.status(401).json();
        }
        req.sanitizeParams("id").toString();
        this.resource.findOneAndUpdate({ _id: req.params.id }, { "$set": req.body }, (err, item) => {
            if (err) {
                return next(err);
            }
            if (!item) {
                return res.status(404).json();
            }
            return res.status(200).json();
        });
    }
}
exports.CoreCrudController = CoreCrudController;
//# sourceMappingURL=core-crud.controller.js.map