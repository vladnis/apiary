"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by vlad on 20.05.2017.
 */
const mongoose = require("mongoose");
class Database {
    constructor(host) {
        this.host = host;
    }
    connect(callback) {
        mongoose.connect(this.host, {}, function (err) {
            if (err) {
                console.log("MongoDB connection error. Please make sure MongoDB is running.");
                process.exit();
            }
            return callback(mongoose.connection);
        });
    }
}
exports.Database = Database;
//# sourceMappingURL=database.js.map