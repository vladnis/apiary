"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const DATE_FORMAT_USA = 0;
const DATE_FORMAT_NON_USA = 1;
const MONEY_FORMAT_US_STYLE = 1;
const MONEY_FORMAT_NON_US_STYLE = 2;
const MONEY_SYMBOL_POSITION_BEFORE = 0;
const MONEY_SYMBOL_POSITION_AFTER = 1;
const AppSettingsSchema = new mongoose.Schema({
    installed: {
        type: Boolean,
        required: true
    },
    languages: [
        { type: mongoose_1.Schema.Types.ObjectId, ref: "Languages" }
    ],
    auctionExtend: {
        from: {
            type: Number,
            required: true
        },
        to: {
            type: Number,
            required: true
        }
    },
    initialBidsNumber: {
        type: Number,
        required: true
    },
    aboutUsPage: {
        enabled: {
            type: Boolean,
            required: true
        },
        description: {
            type: String,
            required: true
        }
    },
    howItWorks: {
        enabled: {
            type: Boolean,
            required: true
        },
        description: {
            type: String,
            required: true
        }
    },
    termsAndConditions: {
        enabled: {
            type: Boolean,
            required: true
        },
        description: {
            type: String,
            required: true
        }
    },
    faq: {
        enabled: {
            type: Boolean,
            required: true
        },
        description: {
            type: String,
            required: true
        }
    },
    acceptanceText: {
        enabled: {
            type: Boolean,
            required: true
        },
        description: {
            type: String,
            required: true
        }
    },
    news: {
        enabled: {
            type: Boolean,
            required: true
        }
    },
    dateFormat: {
        type: Number,
        enum: [DATE_FORMAT_USA, DATE_FORMAT_NON_USA],
        required: true
    },
    currency: {
        defaultCurrency: {
            type: Number
        },
        moneyFormat: {
            type: Number,
            enum: [MONEY_FORMAT_US_STYLE, MONEY_FORMAT_NON_US_STYLE],
            required: true
        },
        moneyDecimals: {
            type: Number,
            required: true
        },
        moneySymbolPosition: {
            type: Number,
            enum: [MONEY_SYMBOL_POSITION_BEFORE, MONEY_SYMBOL_POSITION_AFTER],
            required: true
        }
    },
    auctions: {
        showClosedAuctions: {
            type: Boolean,
            required: true
        },
        noAuctionsDisplayed: {
            type: Number,
            required: true
        }
    },
    batchSettings: {
        deleteAuctionsOlderThan: {
            type: Number,
            required: true
        }
    }
});
const AppSettings = mongoose.model("AppSettings", AppSettingsSchema);
exports.default = AppSettings;
//# sourceMappingURL=app-settings.js.map