"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcrypt-nodejs");
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const userSchema = new mongoose.Schema({
    email: {
        unique: true,
        type: String,
        required: true,
        lowercase: true,
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, "Please fill a valid email address"],
        trim: true,
        minlength: 3,
        maxlength: 255,
        validate: {
            validator: function (v) {
                if (this.email !== this._confirmEmail) {
                    this.invalidate("confirmEmail", "Emails do not match.");
                }
            }
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 255,
        validate: {
            validator: function (v) {
                const user = this;
                if (!user.isModified("password")) {
                    return;
                }
                if (this.password !== this._passwordConfirmation) {
                    this.invalidate("passwordConfirmation", "Passwords do not match.");
                }
            }
        }
    },
    passwordResetToken: String,
    passwordResetExpires: Date,
    facebook: String,
    twitter: String,
    google: String,
    tokens: Array,
    username: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        minlength: 2,
        maxlength: 255,
        unique: true,
    },
    permissions: [
        { type: mongoose_1.Schema.Types.ObjectId, ref: "Permission" }
    ],
    activated: {
        type: Boolean,
        required: true
    },
    lastLogin: {
        type: Date
    },
    dummy: {
        type: Boolean
    }
}, {
    timestamps: true
});
userSchema.virtual("passwordConfirmation")
    .get(function () {
    return this._passwordConfirmation;
})
    .set(function (value) {
    this._passwordConfirmation = value;
});
/**
 * Password hash middleware.
 */
userSchema.pre("save", function save(next) {
    const user = this;
    if (!user.isModified("password")) {
        return next();
    }
    bcrypt.genSalt(10, (err, salt) => {
        if (err) {
            return next(err);
        }
        bcrypt.hash(user.password, salt, undefined, (err, hash) => {
            if (err) {
                return next(err);
            }
            user.password = hash;
            next();
        });
    });
});
userSchema.virtual("confirmEmail")
    .get(function () {
    return this._confirmEmail;
})
    .set(function (value) {
    this._confirmEmail = value;
});
/**
 * Password hash middleware.
 */
userSchema.pre("update", function save(next) {
    const update = this.getUpdate().$set;
    if (!update.password) {
        return next();
    }
    bcrypt.genSalt(10, (err, salt) => {
        if (err) {
            return next(err);
        }
        bcrypt.hash(update.password, salt, undefined, (err, hash) => {
            if (err) {
                return next(err);
            }
            update.password = hash;
            next();
        });
    });
});
userSchema.methods.comparePassword = function (candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        cb(err, isMatch);
    });
};
userSchema.methods.hasPermissions = function (permission) {
    if (!this.permissions) {
        return false;
    }
    return this.permissions.indexOf(permission) !== -1;
};
const User = mongoose.model("User", userSchema);
exports.default = User;
//# sourceMappingURL=user.js.map