"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const user_1 = require("./user");
const bidUserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true,
        minlength: 2,
        maxlength: 255
    },
    surname: {
        type: String,
        required: true,
        trim: true,
        minlength: 2,
        maxlength: 255
    },
    birthdate: {
        type: Date,
        required: true
    },
    balance: {
        type: Number,
        required: true,
    },
    auctionsParticipated: [{
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: "Auctions"
        }],
    watchListAuctionIds: [{
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        }],
    creditsUsed: {
        type: Number,
        required: true,
    },
    creditsBought: {
        type: Number,
        required: true,
    }
}, {
    timestamps: true
});
const BidUser = user_1.default.discriminator("BidUser", bidUserSchema);
exports.default = BidUser;
//# sourceMappingURL=bid-user.js.map