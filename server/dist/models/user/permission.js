"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
class Permissions {
}
Permissions.PERMISSION_ADMIN = "admin";
exports.Permissions = Permissions;
const permissionSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
});
const Permission = mongoose.model("Permission", permissionSchema);
exports.default = Permission;
//# sourceMappingURL=permission.js.map