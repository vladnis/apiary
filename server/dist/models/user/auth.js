"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const authSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 255
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 255
    }
}, {
    timestamps: true
});
const Auth = mongoose.model("Auth", authSchema);
exports.default = Auth;
//# sourceMappingURL=auth.js.map