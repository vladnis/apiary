"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const user_1 = require("./user");
const adminUserSchema = new mongoose.Schema({});
const AdminUser = user_1.default.discriminator("AdminUser", adminUserSchema);
exports.default = AdminUser;
//# sourceMappingURL=admin-user.js.map